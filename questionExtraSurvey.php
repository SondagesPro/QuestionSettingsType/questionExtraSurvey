<?php

/**
 * questionExtraSurvey use a question to add survey inside survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2025 Denis Chenu <www.sondages.pro>
 * @copyright 2017-2025 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license AGPL v3
 * @version 5.13.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class questionExtraSurvey extends PluginBase
{
    protected static $name = 'questionExtraSurvey';
    protected static $description = 'Add survey inside survey : need a survey not anonymous and with token table for the 2 surveys.';

    protected $storage = 'DbStorage';

    /**
    * @var array[] the settings
    */
    protected $settings = array(
        'usageList' => array(
            'type' => 'info',
            'content' => 'Minimum rights are survey read',
        ),
    );

    /**
     * actual qid for this survey
     */
    private $qid;

    /**
     * @var integer the DB version needed
     */
    private static $DBversion = 1;

    /**
     * Keep some information between events
     */
    /* @var string|null $surveyState */
    private $surveyState = null;
    /* @var integer|null $currentSid */
    private $currentSid = null;
    /* @var integer|null $currentSrid */
    private $currentSrid = null;

    /* @var PluginEvent $renderEvent */
    private $renderEvent = null;

    /* @var integer : the previous survey value */
    private $previousSurvey = '';

    /**
     * @var booleand, unsure script not done multiple time.
     */
    private $globalScriptDone = false;

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        Yii::setPathOfAlias('questionExtraSurvey', dirname(__FILE__));

        $this->subscribe('beforeQuestionRender');
        $this->subscribe('newQuestionAttributes', 'addExtraSurveyAttribute');

        $this->subscribe('beforeSurveyPage');

        $this->subscribe('newDirectRequest');

        $this->subscribe('afterSurveyComplete');

        /* Check if user have sufficient right on related survey */
        $this->subscribe('beforeQuestionAttributeSave');
        if (intval(App()->getConfig('versionnumber')) <= 3) {
            /* We are in 3.X and lesser : attribute was deleted before save again */
            $this->subscribe('beforeControllerAction');
        }
        /* in 5.4.9 : newQuestionAttributes multiple call, avoid DB request */
        $this->subscribe('afterPluginLoad');
    }

    public function afterPluginLoad()
    {
        $this->fixDbByVersion();
    }

    /**
     * Add the list of used survey link
     * @see parent:getPluginSettings
     */
    public function getPluginSettings($getValues = true)
    {
        $pluginSettings = parent::getPluginSettings($getValues);
        if (!App()->getController() || App()->getController()->getId() != 'admin') {
            return $pluginSettings;
        }
        if (!App()->getController()->getAction() || App()->getController()->getAction()->getId() != 'pluginmanager') {
            return $pluginSettings;
        }
        if (Yii::app()->request->getParam('sa') != 'configure') {
            return $pluginSettings;
        }
        if (!Permission::model()->hasGlobalPermission('surveys')) {
            return $pluginSettings;
        }
        $dataViews = array();
        $oQuestionExtraSurveys = QuestionAttribute::model()->findAll(array(
            'condition' => "attribute = :attribute and (value is not null and value <> '')",
            'params' => array(":attribute" => 'extraSurvey'),
            'order' => 'value ASC'
        ));
        $aQidSurveys = array();
        foreach ($oQuestionExtraSurveys as $oQuestionExtraSurvey) {
            $data = array(
                'qid' => $oQuestionExtraSurvey->qid,
                'gid' => null,
                'sid' => null,
                'sidname' => "",
                'withsid' => trim($oQuestionExtraSurvey->value),
                'withsidname' => "",
            );
            $oQuestion = Question::model()->find('qid = :qid', array(":qid" => $oQuestionExtraSurvey->qid));
            if ($oQuestion) {
                $data['sid'] = $oQuestion->sid;
                $data['gid'] = $oQuestion->gid;
                $oSurveyQid = Survey::model()->findByPk($oQuestion->sid);
                if ($oSurveyQid) {
                    $data['sidname'] = $oSurveyQid->getLocalizedTitle();
                }
            }
            $oSurveyRelated = Survey::model()->findByPk(trim($oQuestionExtraSurvey->value));
            if ($oSurveyRelated) {
                $data['withsidname'] = $oSurveyRelated->getLocalizedTitle();
            }
            $aQidSurveys[$oQuestionExtraSurvey->qid] = $data;
        }
        $dataViews['aQidSurveys'] = $aQidSurveys;
        $dataViews['surveylink'] = array(
            'surveyAdministration/view'
        );
        $dataViews['questionlink'] = array(
            'questionAdministration/view'
        );
        if (App()->getConfig('versionnumber') < 4) {
            $dataViews['surveylink'] = array(
                'admin/survey/sa/view'
            );
            $dataViews['questionlink'] = array(
                'admin/questions/sa/view'
            );
        }
        $pluginSettings['usageList']['content'] = $this->renderPartial('admin.usage', $dataViews, true);
        return $pluginSettings;
    }

    /**
     * get previous survey value in 3.X
     */
    public function beforeControllerAction()
    {
        $controllerActionEvent = $this->getEvent();
        if ($controllerActionEvent->get('controller') != 'admin') {
            return;
        }
        if ($controllerActionEvent->get('action') != 'database') {
            return;
        }
        if (empty(App()->getRequest()->getpost('extraSurvey'))) {
            return;
        }
        $qid = App()->getRequest()->getpost('qid');
        if (empty($qid)) {
            return;
        }
        $oExtraSurvey = QuestionAttribute::model()->find(
            "qid = :qid and attribute = :attribute",
            [":qid" => $qid, ":attribute" => 'extraSurvey']
        );
        if (empty($oExtraSurvey)) {
            return;
        }
        $this->previousSurvey = $oExtraSurvey->value;
    }

    /**
     * The extra survey attributes
     */
    public function addExtraSurveyAttribute()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $extraAttributes = array(
            'extraSurvey' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 10, /* Own category */
                'inputtype' => 'integer',
                'min' => 0,
                'default' => '',
                'help' => $this->translate('The survey id to be used, you need permission on this survey.'),
                'caption' => $this->translate('Extra survey id'),
            ),
            'extraSurveyQuestionLink' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 20, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->translate('The question code in the extra survey to be used. If empty : only token or optionnal fields was used for the link.'),
                'caption' => $this->translate('Question for response id'),
            ),
            'extraSurveyQuestionLinkUse' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 21, /* Own category */
                'inputtype' => 'switch',
                'default' => 1,
                'help' => $this->translate('Choose if you want only related to current response. If survey use token persistence and allow edition, not needed and id can be different after import an old response database.'),
                'caption' => $this->translate('Get only response related to current response id.'),
            ),
            'extraSurveyMultipleLinks' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 22, /* Own category */
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->translate('When allowing multiple links : user can add link to existing response, this need both previous settings at set.'),
                'caption' => $this->translate('Allow multiple links.'),
            ),
            'extraSurveyMultipleLinksBy' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 24, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->translate('By default use current response id. You can use another data here. You can  use only simple alphanumeric value here.'),
                'caption' => $this->translate('Multiple links relation if not current response id'),
            ),
            /* TODO : See help and detail accrodng to token table and to anonymous ... */
            'extraSurveyTokenUsage' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 30, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'no' => gT('No'),
                    'token' => gT('Yes'),
                    'group' => gT('Token Group (with responseListAndManage plugin)')
                ),
                'default' => 'token',
                'help' => $this->translate('If you have responseListAndManage, the response list can be found using the group of current token. If the related survey have a token table : token stay mandatory.'),
                'caption' => $this->translate('Usage of token.'),
            ),
            'extraSurveyDisableReloadSettings' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 40, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'default' => 0,
                'help' => $this->translate('By default reload response plugin settings are forced according to this option, you can disable this here and leave reloadAnyResponse default action.'),
                'caption' => $this->translate('Disable forced reload response settings.'),
            ),
            'extraSurveyQuestion' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 50, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->translate('This can be text question type, single choice question type or equation question type.'),
                'caption' => $this->translate('Question code for listing.'),
            ),
            'extraSurveyQuestionLinkCreate' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 51, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->translate('This can be text question type, b y default use question code for listing. used when user don\'t found text entered to create a new response in case of mumltiple links allowed.'),
                'caption' => $this->translate('Question code for prefilling (link).'),
            ),
            'extraSurveyAutoDelete' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 60, /* Own category */
                'inputtype' => 'switch',
                'default' => 1,
                'help' => $this->translate('When dialog box is closed, the new survey was checked, if title is empty : survey was deleted. This doesn\'t guarantee you never have empty title, just less.'),
                'caption' => $this->translate('Delete survey without title when close.'),
            ),
            'extraSurveyOtherField' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 70, /* Own category */
                'inputtype' => 'textarea',
                'default' => "",
                'expression' => 1,
                'help' => $this->translate('One field by line, field must be a valid question code, single question only for a real relation. The value is set when question is shown for the 1st time, variable mus be static. Field and value are separated by colon (<code>:</code>), you can use Expressiona Manager in value.'),
                'caption' => $this->translate('Other question fields for relation.'),
            ),
            'extraSurveyQuestionAllowDelete' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 80, /* Own category */
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->translate("Add a button to delete inside modal box, this allow user to really delete the reponse."),
                'caption' => $this->translate('Allow delete response.'),
            ),
            'extraSurveyDeleteUnsubmitted' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 90, /* Own category */
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->translate("If a survey is unsubmitted : disallow close of dialog before submitting."),
                'caption' => $this->translate('Disallow close without submit.'),
            ),
            'extraSurveyFillAnswer' => array(
                'types' => 'T',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 90, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'listall' => $this->translate('List of all answers.'),
                    'listsubmitted' => $this->translate('List of submitted answers.'),
                    'number' => $this->translate('Float value with integer as submitted and decimal as not submitted.'),
                    'separator' => $this->translate('Number of submitted and not submitted answers separate by <code>|</code>.'),
                ),
                'default' => 'number',
                'help' => $this->translate('Recommended method is separator : submitted answer at left part, and not submitted as right part (<code>submitted[|not-submitted]</code>).You can check if all answer are submitted with <code>intval(self)==self</code>.'),
                'caption' => $this->translate('Way for filling the answer.'),
            ),
            'extraSurveyShowId' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 100, /* Own category */
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->translate('This shown survey without title too.'),
                'caption' => $this->translate('Show id at end of string.'),
            ),
            'extraSurveyOrderBy' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 110, /* Own category */
                'inputtype' => 'text',
                'default' => "",
                'help' => sprintf($this->translate('You can use %sSGQA identifier%s or question code if you have getQuestionInformation plugin for the columns to be ordered. The default order is ASC, you can use DESC. You can use <code>,</code> for multiple order.'), '<a href="https://manual.limesurvey.org/SGQA_identifier" target="_blank">', '</a>'),
                'caption' => $this->translate('Order by (default “id DESC”, “datestamp ASC” for datestamped surveys)'),
            ),
            'extraSurveyExtraQuestionInView' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 190, /* Own category */
                'inputtype' => 'text',
                'default' => "",
                'expression' => 1,
                'help' => sprintf(
                    $this->translate('When generate the response list : you can replace the default view by a twig file in your survey there directory (%s).'),
                    '<code>/survey/questions/answer/extrasurvey/reponseslist.twig</code>'
                ),
                'caption' => $this->translate('Other question to be added in view.'),
            ),
            'extraSurveyNameInLanguage' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 200, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'i18n' => true,
                'expression' => 1,
                'help' => $this->translate('Default to “response“ (translated)'),
                'caption' => $this->translate('Show response as'),
            ),
            'extraSurveyAddNewInLanguage' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 210, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'i18n' => true,
                'expression' => 1,
                'help' => $this->translate('Default to “Add new response”, where response is the previous parameter (translated)'),
                'caption' => $this->translate('Add new line text'),
            ),
            'extraSurveyAutoCloseSubmit' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 220, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'addjs' => $this->translate('Add information and close dialog box'),
                    'add' => $this->translate('Add information'),
                    'js' => $this->translate('Only close dialog box'),
                    'none' => $this->translate('No update and no javascript'),
                ),
                'default' => 'addjs',
                'i18n' => false,
                'expression' => 0,
                'help' => $this->translate('Dialog box are closed using javascript solution. Close after save use autoSaveAndQuit plugin.'),
                'caption' => $this->translate('Auto close when survey is submitted, cleared or closes after save.'),
            ),
            'extraSurveyMaxresponse' => array(
                'types' => 'XT',
                'category' => $this->translate('Extra survey'),
                'sortorder' => 230, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'i18n' => false,
                'expression' => 1,
                'help' => $this->translate('Show the add button until this value is reached. This do not disable adding response by other way. You can use Expression.'),
                'caption' => $this->translate('Maximum reponse.'),
            ),
        );
        if (Yii::getPathOfAlias('getQuestionInformation')) {
            $extraAttributes['extraSurveyOrderBy']['help'] = sprintf(
                $this->translate('You can use %sexpression manager variables%s (question title for example) for the value to be ordered. For the order default is ASC, you can use DESC.'),
                '<a href="https://manual.limesurvey.org/Expression_Manager_-_presentation#Access_to_variables" target="_blank">',
                '</a>'
            );
            $extraAttributes['extraSurveyExtraQuestionInView']['help'] .= $this->translate("You can use expression manager directly with question code separated by ,.");
        } else {
            $extraAttributes['extraSurveyExtraQuestionInView']['help'] .= $this->translate("You can use only single choice question or SGQA separated by ,.");
        }
        if (method_exists($this->getEvent(), 'append')) {
            $this->getEvent()->append('questionAttributes', $extraAttributes);
        } else {
            $questionAttributes = (array)$this->event->get('questionAttributes');
            $questionAttributes = array_merge($questionAttributes, $extraAttributes);
            $this->getEvent()->set('questionAttributes', $questionAttributes);
        }
    }

    /**
     * Control when save
     */
    public function beforeQuestionAttributeSave()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        if (Permission::model()->hasGlobalPermission('surveys', 'update')) {
            return;
        }
        $model = $this->getEvent()->get('model');
        if ($model->getAttribute('attribute') != 'extraSurvey') {
            return;
        }
        $model->setAttribute('value', trim($model->getAttribute('value')));
        if (empty($model->getAttribute('value'))) {
            return;
        }
        $sid = $model->getAttribute('value');
        /* Find if same than previous*/
        $previous = $this->previousSurvey;
        if (!empty($model->getAttribute('qaid'))) {
            $previousModel = QuestionAttribute::model()->findByPk($model->getAttribute('qaid'));
            if ($previousModel) {
                $previous = trim($previousModel->getAttribute('value'));
            }
        }

        if ($previous == $model->getAttribute('value')) {
            return;
        }
        if (Permission::model()->hasSurveyPermission($sid, 'responses', 'update')) {
            return;
        }
        $model->setAttribute('value', $previous);
        App()->setFlashMessage(sprintf(gT("You do not have sufficient right on survey %s"), $sid), 'warning');
    }

    /**
     * Add extra script information during twig
     */
    public function addHiddenInputInBeforeCloseHtml()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $htmlEvent = $this->getEvent();
        $surveyId = $this->currentSid ;
        if (empty($surveyId)) {
            return;
        }
        $currentSrid = isset($_SESSION['survey_' . $surveyId]['srid']) ? $_SESSION['survey_' . $surveyId]['srid'] : null;
        if (empty($currentSrid)) {
            return;
        }
        $html = Chtml::hiddenField(
            'sridQuestionExtraSurvey',
            $currentSrid,
            array(
                'disable' => true,
                'id' => 'sridQuestionExtraSurvey'
            )
        );
        $htmlEvent->set('html', $htmlEvent->get('html') . $html);
    }

    /**
     * Access control on survey
     */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->fixDbByVersion();
        $surveyId = $this->event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            return;
        }
        /* @var object : shortcut for request */
        $request = App()->getrequest();
        $aSessionExtraSurvey = (array) Yii::app()->session["questionExtraSurvey"];
        if ($request->getQuery('newtest') == 'Y') {
            unset($aSessionExtraSurvey[$surveyId]);
        }
        if ($request->getQuery('srid') && $request->getParam('extrasurveyqid')) {
            $extrasurveyqid = $request->getParam('extrasurveyqid');
            if ($this->validateQuestionExtraSurvey($request->getParam('extrasurveyqid'), $surveyId)) {
                $this->qid = $request->getParam('extrasurveyqid');
                $aSessionExtraSurvey[$surveyId] = $request->getParam('extrasurveyqid');
            } else {
                \Yii::log("Invalid qid {$extrasurveyqid} checked for survey {$surveyId}", \CLogger::LEVEL_WARNING, 'plugin.questionExtraSurvey.beforeSurveypage');
            }
        }
        if ($request->getPost('questionExtraSurveyQid')) { // From javascipt modal buttons
            $extrasurveyqid = $request->getParam('extrasurveyqid');
            if ($this->validateQuestionExtraSurvey($extrasurveyqid, $surveyId)) {
                $aSessionExtraSurvey[$surveyId] = $extrasurveyqid;
            }
        }
        Yii::app()->session["questionExtraSurvey"] = $aSessionExtraSurvey;
        /* register needed script for external survey */
        if (empty(Yii::app()->session["questionExtraSurvey"][$surveyId])) {
            return;
        }
        $this->subscribe('beforeCloseHtml', 'addHiddenInputInBeforeCloseHtml');
        $this->currentSid = $surveyId;
        $this->qesRegisterExtraSurveyScript();

        if ($request->getPost('saveall') && $request->getPost('autosaveandquit')) {
            $script = "extraSurveyAutoclose();";
            Yii::app()->getClientScript()->registerScript("questionExtraSurveyComplete", $script, CClientScript::POS_END);
        }

        if ($request->getPost('saveall') && Survey::model()->findByPk($surveyId)->allowsave == "Y") {
            $isSaveandQuit = false;
            if (Yii::getPathOfAlias('autoSaveAndQuit')) {
                $isSaveandQuit = \autoSaveAndQuit\Utilities::isSaveAndQuit($surveyId) && !\autoSaveAndQuit\Utilities::isDisableSaveAndQuit($surveyId);
            }
            /* Quit if is save and quit except for automatic one */
            if ($isSaveandQuit && !$request->getPost("saveandquit-autosave")) {
                $script = "extraSurveyAutoclose();";
                Yii::app()->getClientScript()->registerScript("questionExtraSurveyComplete", $script, CClientScript::POS_END);
            }
        }
        if ($request->getPost('clearall') == "clearall" && $request->getPost('confirm-clearall')) {
            $script = "extraSurveyAutoclose();";
            Yii::app()->getClientScript()->registerScript("questionExtraSurveyComplete", $script, CClientScript::POS_END);
        }
        /* 2 way to set clearall : add a delete to POST, or questionExtraSurvey-delete with confirm */
        if (
            ($request->getPost('clearall') == "clearall" && $request->getPost('delete') == 'delete')
            || ($request->getPost('ExtraSurvey-delete') == "delete" && $request->getPost('ExtraSurvey-delete-confirm'))
        ) {
            $this->checkDeletion($surveyId);
        }
    }

    /**
     * Check if delete is allowed and add the deletion on twig part
     * @param integer $surveyId;
     * @return void
     */
    private function checkDeletion($surveyId)
    {
        $qid = Yii::app()->session["questionExtraSurvey"][$surveyId];
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        $reloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId);
        if ($srid != $reloadedSrid) {
            $this->log(sprintf("Try to delete %s whith reloaded %s in survey %s.", $srid, $reloadedSrid, $surveyId), 'info');
            return;
        }
        $extraSurveyQuestionAllowDelete = QuestionAttribute::model()->find(
            'qid = :qid AND attribute = :attribute',
            array(":qid" => $qid, ':attribute' => 'extraSurveyQuestionAllowDelete')
        );
        if (!$extraSurveyQuestionAllowDelete || !$extraSurveyQuestionAllowDelete->value) {
            $this->log(sprintf("Try to delete %s whith invalid question %s in survey %s.", $srid, $qid, $surveyId), 'warning');
            return;
        }
        /* Disable default clearall */
        $_POST['clearall'] = "";
        $_POST['confirm-clearall'] = "";
        $this->unsubscribe('getPluginTwigPath'); /* Other not needed */
        $this->currentSrid = $srid;
        $this->subscribe('getPluginTwigPath', 'deleteAllAction');
    }

    /**
     * Registred on twig path : doing action of deleting all
     * @retun void
     */
    public function deleteAllAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->unsubscribe('getPluginTwigPath');
        $surveyId = $this->currentSid;
        $currentSrid = $this->currentSrid;
        if (!$surveyId || !$currentSrid) {
            throw new CHttpException(500);
        }
        $oResponse = Response::model($surveyId)->findByPk($currentSrid);
        if (empty($oResponse)) {
            return;
        }
        $oCriteria = new CDbCriteria();
        $oCriteria->compare('id', $currentSrid);
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($this->currentSid);
            $RelatedSurveysHelper->deleteResponse($oCriteria);
        } else {
            $oResponse->deleteAll($oCriteria);
        }
        killSurveySession($surveyId);
        $aSessionExtraSurvey = Yii::app()->session["questionExtraSurvey"];
        unset($aSessionExtraSurvey[$surveyId]);
        Yii::app()->session["questionExtraSurvey"] = $aSessionExtraSurvey;
        $aSurveyinfo = getSurveyInfo($surveyId, App()->getLanguage());
        $aSurveyinfo['include_content'] = 'questionExtraSurvey_delete';
        $this->subscribe('getPluginTwigPath');
        $script = "extraSurveyAutoclose();";
        Yii::app()->getClientScript()->registerScript("questionExtraSurveyComplete", $script, CClientScript::POS_END);
        Yii::app()->twigRenderer->renderTemplateFromFile(
            "layout_global.twig",
            array(
                'oSurvey' => Survey::model()->findByPk($surveyId),
                'aSurveyInfo' => $aSurveyinfo
            ),
            false
        );
    }

    public function afterSurveyComplete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $afterSurveyCompleteEvent = $this->getEvent();
        $surveyId = $afterSurveyCompleteEvent->get('surveyId');
        $currentSrid = $afterSurveyCompleteEvent->get('responseId');
        $aSessionExtraSurvey = Yii::app()->session["questionExtraSurvey"];
        if (!isset($aSessionExtraSurvey[$surveyId])) {
            /* Quit if we are not in survey inside surey system */
            return;
        }
        $currentQid = $aSessionExtraSurvey[$surveyId];
        $responseText = mb_strtolower($this->translate("Response"), 'UTF-8');
        $oAttributeReponseText = QuestionAttribute::model()->find(
            "qid = :qid AND language = :language AND attribute = :attribute",
            array(
                ":qid" => $currentQid,
                ":language" => Yii::app()->getLanguage(),
                ":attribute" => 'extraSurveyNameInLanguage'
            )
        );
        if ($oAttributeReponseText && $oAttributeReponseText->value) {
            $responseText = $oAttributeReponseText->value;
        }
        unset($aSessionExtraSurvey[$surveyId]);
        $oAttributeExtraSurveyAutoCloseSubmit = QuestionAttribute::model()->find(
            "qid = :qid AND attribute = :attribute",
            array(
                ":qid" => $currentQid,
                ":attribute" => 'extraSurveyAutoCloseSubmit'
            )
        );
        $extraSurveyAutoCloseSubmit = "addjs";
        if ($oAttributeExtraSurveyAutoCloseSubmit) {
            $extraSurveyAutoCloseSubmit =  $oAttributeExtraSurveyAutoCloseSubmit->value;
        }
        Yii::app()->session["questionExtraSurvey"] = $aSessionExtraSurvey;
        if (in_array($extraSurveyAutoCloseSubmit, array('addjs', 'js'))) {
            $script = "extraSurveyAutoclose();";
            Yii::app()->getClientScript()->registerScript("questionExtraSurveyComplete", $script, CClientScript::POS_END);
        }
        if (in_array($extraSurveyAutoCloseSubmit, array('js', 'none'))) {
            return;
        }
        $renderData = array(
            'language' => array(
                "Your responses was saved as complete, you can close this windows." => $this->translate("Your responses was saved as complete, you can close this windows."),
                "Your %s was saved as complete, you can close this windows." => $this->translate("Your %s was saved as complete, you can close this windows."),
                "response" => $responseText
            ),
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage()),
        );
        $this->subscribe('getPluginTwigPath');
        $extraContent = Yii::app()->twigRenderer->renderPartial('/subviews/messages/questionExtraSurvey_submitted.twig', $renderData);
        if ($extraContent) {
            $afterSurveyCompleteEvent->getContent($this)
                ->addContent($extraContent);
        }
    }

    /**
     * Add the script when question is rendered
     * Add QID and SGQ replacement forced (because it's before this was added by core
     */
    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->renderEvent = $this->getEvent();
        $aQuestionAttributes = QuestionAttribute::model()->getQuestionAttributes($this->renderEvent->get('qid'), Yii::app()->getLanguage());
        $surveyId = $this->renderEvent->get('surveyId');
        if (isset($aQuestionAttributes['extraSurvey']) && trim($aQuestionAttributes['extraSurvey'])) {
            $this->registerPackage();
            $token = Yii::app()->getRequest()->getParam('token');
            if (empty($token)) {
                $token = !empty(Yii::app()->session["survey_$surveyId"]['token']) ? Yii::app()->session["survey_$surveyId"]['token'] : null;
            }
            $thisSurvey = Survey::model()->findByPk($surveyId);
            $extraSurveyAttribute = trim($aQuestionAttributes['extraSurvey']);
            $extraSurvey = Survey::model()->findByPk($extraSurveyAttribute);
            $disableMessage = "";
            if (!$extraSurvey) {
                $disableMessage = sprintf($this->translate("Invalid survey %s for question %s."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
            }
            if (!$disableMessage && $extraSurvey->active != "Y") {
                $disableMessage = sprintf($this->translate("Survey %s for question %s not activated."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
            }
            if (!$disableMessage && !$this->surveyAccessWithToken($thisSurvey) && $this->surveyAccessWithToken($extraSurvey)) {
                $disableMessage = sprintf($this->translate("Survey %s for question %s can not be used with a survey without tokens."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
            }
            if (!$disableMessage && $this->surveyAccessWithToken($thisSurvey) && $extraSurvey->anonymized == "Y") {
                $disableMessage = sprintf($this->translate("Survey %s for question %s need to be not anonymized."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
            }
            if (!$disableMessage && $this->surveyAccessWithToken($thisSurvey) && $extraSurvey->anonymized == "Y") {
                $disableMessage = sprintf($this->translate("Survey %s for question %s need to be not anonymized."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
            }
            if (!$disableMessage && $this->surveyAccessWithToken($extraSurvey) && Yii::app()->getConfig('previewmode') && !Yii::app()->request->getQuery('token')) {
                $disableMessage = sprintf($this->translate("Survey %s for question %s can not be loaded in preview mode without a valid token."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
            }
            if (!$disableMessage && $this->surveyAccessWithToken($extraSurvey)) {
                if (!$this->validateToken($extraSurvey, $thisSurvey, $token)) {
                    $disableMessage = sprintf($this->translate("Survey %s for question %s token can not ne found or created."), $extraSurveyAttribute, $this->renderEvent->get('qid'));
                }
            }

            if ($disableMessage) {
                $this->renderEvent->set("answers", CHtml::tag("div", array('class' => 'alert alert-warning'), $disableMessage));
                return;
            }
            $this->subscribe('getPluginTwigPath');
            $this->setSurveyListForAnswer($extraSurvey->sid, $aQuestionAttributes, $token);
            $this->setReloadAnyResponseAnswer($extraSurvey->sid, $aQuestionAttributes);
            $this->unsubscribe('getPluginTwigPath');
        }
    }

    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        if ($oEvent->get('target') != get_class()) {
            return;
        }
        /* @var int The destination survey */
        $surveyId = $this->api->getRequest()->getParam('surveyid');
        /* @var string token */
        $token = $this->api->getRequest()->getParam('token');
        /* @var integer question id */
        $qid = $this->api->getRequest()->getParam('qid');
        $lang = $this->api->getRequest()->getParam('lang');
        if (!$surveyId || !$qid) {
            return;
        }
        $oAttributeExtraSurvey = QuestionAttribute::model()->find('attribute=:attribute AND qid=:qid', array(
            ':attribute' => 'extraSurvey',
            ':qid' => $qid,
        ));
        if (!$oAttributeExtraSurvey || $oAttributeExtraSurvey->value != $surveyId) {
            throw new CHttpException(403, "Invalid parameters");
        }
        $oExtraSurvey = Survey::model()->findByPk($surveyId);
        if (!in_array($lang, $oExtraSurvey->getAllLanguages())) {
            $lang = $oExtraSurvey->language;
        }
        App()->setLanguage($lang);
        $oQuestion = Question::model()->find([
            'select' => 'sid',
            'condition' => "qid = :qid",
            'params' => [':qid' => $qid]
        ]);
        $sessionEmSurvey = App()->getSession()->get("survey_{$oQuestion->sid}");
        if (empty($sessionEmSurvey)) {
            throw new CHttpException(403, "Invalid survey");
        }
        $srid = null;
        if (!empty($sessionEmSurvey['srid'])) {
            /* Check if survey is started ? Or user have Permission ? */
            $srid = $sessionEmSurvey['srid'];
        }
        $sAction = $oEvent->get('function');
        switch ($sAction) {
            case 'update':
                /* search if it's a related survey */
                if ($oAttributeExtraSurvey && ($oAttributeExtraSurvey->value == $surveyId)) {
                    LimeExpressionManager::SetSurveyId($oQuestion->sid);
                    echo $this->getHtmlPreviousResponse($surveyId, $srid, $qid, $token, $lang, true);
                    break;
                }
                break;
            case 'check':
                /* search if it's a related survey */
                $oAttributeExtraSurvey = QuestionAttribute::model()->find('attribute=:attribute AND qid=:qid', array(
                    ':attribute' => 'extraSurvey',
                    ':qid' => $qid,
                ));
                if ($oAttributeExtraSurvey && ($oAttributeExtraSurvey->value == $surveyId)) {
                    $result = array();
                    $sessionId = \reloadAnyResponse\models\surveySession::getSessionId();
                    if ($sessionId) {
                        $deleted = \reloadAnyResponse\models\surveySession::model()->deleteAll(
                            "sid = :sid and session = :session",
                            array(':sid' => $surveyId, ':session' => $sessionId)
                        );
                        $result['sessiondeleted'] = $deleted;
                    }
                    $sridToCheck = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId);
                    if (empty($sridToCheck)) {
                        $sridToCheck = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
                    }
                    if (empty($sridToCheck)) {
                        return;
                    }
                    if (empty($deleted)) {
                        $count = \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId,'srid' => $sridToCheck));
                        $result['sriddeleted'] = $count;
                    }
                    $result['titledeleted'] = $this->checkIfTitleIsEmpty($qid, $surveyId, $sridToCheck);
                    header('Content-type: application/json; charset=utf-8');
                    echo json_encode($result);
                    Yii::app()->end();
                }
                break;
            case 'searchlink':
                return $this->returnSearchResponseListForLink($qid, $oQuestion->sid, $surveyId, App()->getRequest()->getParam('term'));
                break;
            case 'dolink':
                return $this->actionDoLink($qid, $oQuestion->sid, $surveyId, App()->getRequest()->getParam('linksrid'));
                break;
            case 'unlink':
                return $this->actionUnLink($qid, $oQuestion->sid, $surveyId, App()->getRequest()->getParam('linksrid'));
                break;
            default:
                throw new CHttpException(400, "Invalid function $sAction");
            // Nothing to do (except log error)
        }
    }

    /**
     * Check if the final title is empty, delete if true (accordinng to setting)
     * @param integer $qid questio id source
     * @param $sridToCheck : the srid of extra survey
     * @return null|integer
     */
    private function checkIfTitleIsEmpty($qid, $extraSurveyId, $sridToCheck)
    {
        $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
        if (empty($oQuestion)) {
            return;
        }
        if (!$this->validateQuestionExtraSurvey($qid, $extraSurveyId)) {
            return;
        }
        $thisSurveyId = $oQuestion->sid;
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['extraSurveyQuestion'])) {
            return;
        }
        if (empty($aAttributes['extraSurveyAutoDelete'])) {
            return;
        }
        $qCodeText = trim($aAttributes['extraSurveyQuestion']);
        if (empty($qCodeText)) {
            return;
        }

        /* validate 2 survey … TODO ! move to a new function */
        $thisSurvey = Survey::model()->findByPk($extraSurveyId);
        $extraSurvey = Survey::model()->findByPk($extraSurveyId);
        if (!$extraSurvey) {
            return;
        }
        if ($extraSurvey->active != "Y") {
            return;
        }
        if (!$this->surveyAccessWithToken($thisSurvey) && $this->surveyAccessWithToken($extraSurvey)) {
            return;
        }
        if ($this->surveyAccessWithToken($thisSurvey) && $extraSurvey->anonymized == "Y") {
            return;
        }
        if ($this->surveyAccessWithToken($extraSurvey) && Yii::app()->getConfig('previewmode') && !Yii::app()->request->getQuery('token')) {
            return;
        }
        /* Validate what we need */
        $token = isset($_SESSION["survey_$thisSurveyId"]['token']) ? $_SESSION["survey_$thisSurveyId"]['token'] : null;
        $srid = isset($_SESSION["survey_$thisSurveyId"]['srid']) ? $_SESSION["survey_$thisSurveyId"]['srid'] : null;
        /* Search and delete */
        $qCodeSrid = trim($aAttributes['extraSurveyQuestionLink']);

        $relatedTokens = $aAttributes['extraSurveyTokenUsage'] == 'group';
        $aOtherFields = $this->getOtherField($qid);
        if (!$aAttributes['extraSurveyQuestionLinkUse']) {
            $qCodeSrid = null;
        }
        /* Find the question code */
        $oQuestionText = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $extraSurveyId,":title" => $qCodeText));
        $qCodeText = null;
        if ($oQuestionText && in_array($oQuestionText->type, array("S","T","U","L","!","O","N","D","G","Y","*"))) {
            $qCodeText = "{$oQuestionText->sid}X{$oQuestionText->gid}X{$oQuestionText->qid}";
            $aSelect[] = Yii::app()->db->quoteColumnName($qCodeText);
        }
        if (empty($qCodeText)) {
            return;
        }
        $oCriteria = new CDbCriteria();
        $qQuotesCodeText = Yii::app()->db->quoteColumnName($qCodeText);
        $oCriteria->condition = "$qQuotesCodeText IS NULL OR $qQuotesCodeText = ''";
        if ($token) {
            $tokens = array($token => $token);
            if ($relatedTokens) {
                $tokens = $this->getTokensList($extraSurveyId, $token);
            }
            $oCriteria->addInCondition("token", $tokens);
        }
        if ($qCodeSrid && $srid) {
            $oQuestionSrid = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $extraSurveyId,":title" => $qCodeSrid));
            if ($oQuestionSrid && in_array($oQuestionSrid->type, array("T","S","N"))) {
                $qCodeSrid = "{$oQuestionSrid->sid}X{$oQuestionSrid->gid}X{$oQuestionSrid->qid}";
                $oCriteria->compare(Yii::app()->db->quoteColumnName($qCodeSrid), $srid);
            }
        }
        if (!empty($aOtherFields)) {
            foreach ($aOtherFields as $questionCode => $value) {
                $oQuestionOther = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $extraSurveyId,":title" => $questionCode));
                if ($oQuestionOther && in_array($oQuestionOther->type, array("5","D","G","I","L","N","O","S","T","U","X","Y","!","*"))) {
                    $qCode = "{$oQuestionOther->sid}X{$oQuestionOther->gid}X{$oQuestionOther->qid}";
                    $oCriteria->compare(Yii::app()->db->quoteColumnName($qCode), $value);
                }
            }
        }
        $oCriteria->compare(Yii::app()->db->quoteColumnName("id"), $sridToCheck);
        /* Did add RelatedSurveysHelper system ? */
        return Response::model($extraSurveyId)->deleteAll($oCriteria);
    }
    /**
     * Set the answwer and other parameters for the system
     * @param int $surveyId for answers
     * @param array $qAttributes
     * @param string $token
     * @return void
     */
    private function setSurveyListForAnswer($surveyId, $aQuestionAttributes, $token = null)
    {
        $qid = $this->renderEvent->get('qid');
        $currentSurveyId = $this->renderEvent->get('surveyId');
        $this->renderEvent->set("class", $this->renderEvent->get("class") . " questionExtraSurvey");
        if (!$token && $this->surveyAccessWithToken(Survey::model()->findByPk($currentSurveyId))) {
            $token = (!empty(Yii::app()->session["survey_{$currentSurveyId}"]['token'])) ? Yii::app()->session["survey_{$currentSurveyId}"]['token'] : null;
        }
        $srid = !empty(Yii::app()->session["survey_{$currentSurveyId}"]['srid']) ? Yii::app()->session["survey_{$currentSurveyId}"]['srid'] : null;
        $this->setOtherField($qid, $aQuestionAttributes['extraSurveyOtherField'], $surveyId);
        $listOfReponses = $this->getHtmlPreviousResponse($surveyId, $srid, $this->renderEvent->get('qid'), $token);
        $ajaxUrl = Yii::app()->getController()->createUrl(
            'plugins/direct',
            array(
                'plugin' => 'questionExtraSurvey',
                'function' => 'update',
                'surveyid' => $surveyId,
                'token' => $token,
                'qid' => $this->renderEvent->get('qid'),
                'lang' => Yii::app()->getLanguage(),
            )
        );
        $ajaxCheckUrl = Yii::app()->getController()->createUrl(
            'plugins/direct',
            array(
                'plugin' => 'questionExtraSurvey',
                'function' => 'check',
                'surveyid' => $surveyId,
                'token' => $token,
                'qid' => $this->renderEvent->get('qid'),
                'lang' => Yii::app()->getLanguage(),
            )
        );
        $oSurveyFrame = Survey::model()->findByPk($surveyId);
        $reponseName = empty($aQuestionAttributes['extraSurveyNameInLanguage'][Yii::app()->getLanguage()]) ? mb_strtolower($this->translate("Response"), 'UTF-8') : $aQuestionAttributes['extraSurveyNameInLanguage'][Yii::app()->getLanguage()];
        /* Find buttons */
        $allowsaveall = false;
        if (Yii::getPathOfAlias('autoSaveAndQuit')) {
            $autoSaveAndQuitActive = \autoSaveAndQuit\Utilities::getSetting($surveyId, 'autoSaveAndQuitActive');
            $autoSaveAndQuitRestrict = \autoSaveAndQuit\Utilities::getSetting($surveyId, 'autoSaveAndQuitRestrict');
            if ($autoSaveAndQuitActive != 'always' || $autoSaveAndQuitRestrict == 'ondemand') {
                $allowsaveall = true;
            }
        }
        $clearAllAction = \reloadAnyResponse\Utilities::getSetting($surveyId, 'clearAllAction');
        $buttons = array(
            'delete' => (bool) $aQuestionAttributes['extraSurveyQuestionAllowDelete'],
            'close' => !(bool)$aQuestionAttributes['extraSurveyDeleteUnsubmitted'],
            //'clearall' => ($clearAllAction == 'reset'), : clearall always save
            'saveall' => ($allowsaveall && $oSurveyFrame->allowsave == "Y"),
            'saveall-quit' => ($oSurveyFrame->allowsave == "Y"),
            'moveprev' => ($oSurveyFrame->allowprev == "Y" && $oSurveyFrame->format != "A"),
            'movenext' => ($oSurveyFrame->format != "A"),
            'movesubmit' => true,
        );

        $modalParams = array(
            'buttons' => $buttons,
            'language' => array(
                'Are you sure you want to delete this response?' => sprintf($this->translate("Are you sure you want to delete this %s?"), $reponseName),
            ),
            'confirmdeletestring' => sprintf($this->translate("Are you sure you want to delete this %s?"), $reponseName),
            'qid' => $this->renderEvent->get('qid'),
            'extrasurveyid' => $surveyId,
        );
        $renderData = array(
            'qid' => $qid,
            'currentSurveyId' => $this->renderEvent->get('surveyId'),
            'surveyId' => $surveyId,
            'extrasurveyqid' => $qid,
            'token' => $token,
            'questionAttributes' => $aQuestionAttributes,
            'questionextrasurveyurl' => $ajaxUrl,
            'questionextrasurveysurvey' => $surveyId,
            'questionextrasurveyclosecheckurl' => $ajaxCheckUrl,
            'questionextrasurveymodalparams' => ls_json_encode($modalParams),
            'questionextrasurveyreponsename' => $reponseName,
            'wrapperOptions' => array(
                'data-update-questionextrasurvey' => $ajaxUrl,
                'data-questionextrasurvey-survey' => $surveyId,
                'data-closecheck-questionextrasurvey' => $ajaxCheckUrl,
                'data-modalparams-questionextrasurvey' => ls_json_encode($modalParams),
                'data-questionextrasurvey-reponsename' => $reponseName
            ),
            'listOfReponses' => $listOfReponses,
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage()),
        );
        $answers = App()->twigRenderer->renderPartial(
            '/survey/questions/answer/extrasurvey/answer.twig',
            $renderData
        );

        $this->renderEvent->set("answers", $answers);
        $this->addGlobalScript($this->renderEvent->get('surveyId'));
    }

    /**
     * Add modal script at end of page with javascript
     * @param integer $surveyId
     * @return void
     */
    private function addGlobalScript($surveyId)
    {
        if ($this->globalScriptDone) {
            return;
        }
        /* Add global options */
        if (!Yii::app()->getClientScript()->isScriptRegistered("questionExtraSurveyGlobalOption", CClientScript::POS_BEGIN)) {
            $questionExtraSurveyJSVar = [
                'options' => [
                    'postBase' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ],
                ],
                'lang' => $this->getLanguageStrings(),
                'autoclickcreate' => false
            ];
            App()->getClientScript()->registerScript(
                "questionExtraSurveyGlobal",
                "LSvar.questionExtraSurvey = $.extend( LSvar.questionExtraSurvey, " . json_encode($questionExtraSurveyJSVar) . ");\n",
                CClientScript::POS_BEGIN
            );
        }
        /* Add the modal */
        $renderData = array(
            'language' => array(
                'Are you sure you want to delete this response?' => sprintf($this->translate("Are you sure you want to delete this %s?"), mb_strtolower(gT("Response"), 'UTF-8')),
                'Yes' => gT("Yes"),
                'No' => gT("No"),
                'Close' => gT("Close"),
                'Delete' => gT("Delete"),
                'Save' => gT("Save"),
                'Save and quit' => $this->translate("Save and quit"),
                'Previous' => gT("Previous"),
                'Next' => gT("Next"),
                'Submit' => gT("Submit"),
            ),
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage()),
        );
        $posReady = CClientScript::POS_READY;
        $this->subscribe('getPluginTwigPath');
        if (!Yii::app()->getClientScript()->isScriptRegistered("questionExtraSurveyModalConfirm", $posReady)) {
            $modalConfirm = Yii::app()->twigRenderer->renderPartial('/subviews/navigation/questionExtraSurvey_modalDeleteConfirm.twig', $renderData);
            Yii::app()->getClientScript()->registerScript("questionExtraSurveyModalConfirm", "$('body').prepend(" . json_encode($modalConfirm) . ");", $posReady);
        }
        if (!Yii::app()->getClientScript()->isScriptRegistered("questionExtraSurveyModalSurvey", $posReady)) {
            $modalSurvey = Yii::app()->twigRenderer->renderPartial('/subviews/navigation/questionExtraSurvey_modalSurvey.twig', $renderData);
            Yii::app()->getClientScript()->registerScript("questionExtraSurveyModalSurvey", "$('body').prepend(" . json_encode($modalSurvey) . ");", $posReady);
        }
        $this->unsubscribe('getPluginTwigPath');
        $this->globalScriptDone = true;
    }

    /**
     * See getPluginTwigPath evcent
     * remind to unsubsribe after usage
     */
    public function getPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * See getValidScreenFiles evcent
     * remind to unsubsribe after usage
     */
    public function getValidScreenFiles()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get("type") != 'view') {
            return;
        }
        $this->subscribe('getPluginTwigPath');
        if (empty($this->getEvent()->get("screen")) || $this->getEvent()->get("screen") == "navigation") {
            $this->getEvent()->append('add', array(
                "subviews" . DIRECTORY_SEPARATOR . "navigation" . DIRECTORY_SEPARATOR . "questionExtraSurvey_modalDeleteConfirm.twig",
                "subviews" . DIRECTORY_SEPARATOR . "navigation" . DIRECTORY_SEPARATOR . "questionExtraSurvey_modalSurvey.twig",
            ));
        }
    }

    /**
     * Set the answwer and other parameters for the system
     * @param int $surveyId the related survey
     * @param int $srid the related identifier
     * @param int $qid the original question id
     * @param null|string $token
     * @param null|string $lang
     * @return string
     */
    private function getHtmlPreviousResponse($surveyId, $srid, $qid, $token = null, $lang = null, $reloaded = false)
    {
        if ($lang) {
            Yii::app()->setLanguage($lang);
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        $inputName = null;
        $oQuestion = Question::model()->find("qid=:qid", array(":qid" => $qid));

        if (in_array($oQuestion->type, array("T","S"))) {
            $inputName = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        }
        if (Survey::model()->findByPk($surveyId)->active != "Y") {
            return "<p class='alert alert-warning'>" . sprintf($this->translate("Related Survey is not activate, related response deactivated."), $surveyId) . "</p>";
        }

        $qCodeText = trim($aAttributes['extraSurveyQuestion']);
        $showId = trim($aAttributes['extraSurveyShowId']);
        $extraSurveyMaxresponse = trim($aAttributes['extraSurveyMaxresponse']);
        if ($extraSurveyMaxresponse) {
            $extraSurveyMaxresponse = trim($this->qesEMProcessString($extraSurveyMaxresponse, true));
        }
        $orderBy = isset($aAttributes['extraSurveyOrderBy']) ? trim($aAttributes['extraSurveyOrderBy']) : null;
        $qCodeSrid = $qCodeSridUsed = trim($aAttributes['extraSurveyQuestionLink']);
        $extraSurveyFillAnswer = $oQuestion->type == 'T' ? trim($aAttributes['extraSurveyFillAnswer']) : "";
        $relatedTokens = $aAttributes['extraSurveyTokenUsage'] == 'group';
        $extraSurveyOtherField = $this->getOtherField($qid);
        if (!$aAttributes['extraSurveyQuestionLinkUse']) {
            $qCodeSridUsed = null;
        }
        $aResponses = $this->getPreviousResponse($surveyId, $srid, $token, $qid);
        $newUrlParam = array(
            'sid' => $surveyId,
            'extrasurveyqid' => $qid,
            'newtest' => 'Y',
            'token' => $token,
            'srid' => 'new',
            'lang' => Yii::app()->getLanguage()
        );
        if (!empty($qCodeSrid)) {
            $newUrlParam[$qCodeSrid] = $srid;
        }
        if (!empty($extraSurveyOtherField)) {
            foreach ($extraSurveyOtherField as $key => $value) {
                $newUrlParam[$key] = $value;
            }
        }
        /* If link as to be created */
        $linkUrl = null;
        if ($aAttributes['extraSurveyMultipleLinks']) {
            $prefillValue = trim($aAttributes['extraSurveyQuestionLinkCreate']);
            if (empty($prefillValue)) {
                $prefillValue = trim($aAttributes['extraSurveyQuestion']);
            }
            $newUrlParam[$prefillValue] = "";
        }
        $linkSearchUrl = Yii::app()->getController()->createUrl(
            'plugins/direct',
            array(
                'plugin' => 'questionExtraSurvey',
                'function' => 'searchlink',
                'surveyid' => $surveyId,
                'token' => $token,
                'qid' => $qid,
                'lang' => Yii::app()->getLanguage(),
            )
        );
        /* Need some information on current Srid */
        $currentSurveyId = $oQuestion->sid;
        $currentStep = isset($_SESSION['survey_' . $currentSurveyId]['step']) ? $_SESSION['survey_' . $currentSurveyId]['step'] : null;
        $reponseName = empty($aAttributes['extraSurveyNameInLanguage'][Yii::app()->getLanguage()]) ? mb_strtolower(gT("Response"), 'UTF-8') : $aAttributes['extraSurveyNameInLanguage'][Yii::app()->getLanguage()];
        $reponseAddNew = empty($aAttributes['extraSurveyAddNewInLanguage'][Yii::app()->getLanguage()]) ? sprintf($this->translate("Add a new %s"), mb_strtolower($reponseName, 'UTF-8')) : $aAttributes['extraSurveyAddNewInLanguage'][Yii::app()->getLanguage()];

        /* Fo JS solution */
        $extraSurveyExtraQuestions = [];
        if (!empty($aAttributes['extraSurveyExtraQuestionInView'])) {
            $extraSurveyExtraQuestions = explode(',', $aAttributes['extraSurveyExtraQuestionInView']);
            $extraSurveyExtraQuestions = array_filter(array_map('trim', $extraSurveyExtraQuestions));
        }
        /* The value */
        $value = "";
        switch ($extraSurveyFillAnswer) {
            case 'listsubmitted':
                $aValidResponse = array_filter($aResponses, function ($aResponse) {
                    return (!empty($aResponse['submitdate']));
                });
                $value = implode(",", array_keys($aValidResponse));
                break;
            case 'listall':
                $value = implode(",", array_keys($aResponses));
                break;
            case 'number':
            case 'separator':
            default:
                $value = count(array_filter($aResponses, function ($aResponse) {
                    return (!empty($aResponse['submitdate']));
                }));
                $notsubmitted = count($aResponses) - $value;
                if ($notsubmitted) {
                    if ($extraSurveyFillAnswer == 'separator') {
                        $value = $value . "|" . $notsubmitted;
                    } else {
                        $value = $value . "." . $notsubmitted;
                    }
                }
                if ($value === "0" or $value === 0) {
                    // Move "0" to "" : mandatory system can still be used.
                    $value = "";
                }
                break;
        }
        $renderData = array(
            'aSurveyInfo' => getSurveyInfo($currentSurveyId, App()->getLanguage()),
            'currentSurveyId' => $currentSurveyId,
            'aResponses' => $aResponses,
            'inputValue' => $value,
            'questionAttributes' => $aAttributes,
            'extraSurveyExtraQuestions' => $extraSurveyExtraQuestions,
            'surveyid' => $surveyId,
            'surveyId' => $surveyId,
            'extrasurveyqid' => $qid,
            'qid' => $qid,
            'token' => $token,
            'newUrl' => App()->getController()->createUrl('survey/index', $newUrlParam),
            'linkSearchUrl' => $linkSearchUrl ,
            'maxResponse' => $extraSurveyMaxresponse,
            'inputName' => $inputName,
            'fillAnswerWith' => $extraSurveyFillAnswer,
            'language' => array_merge(
                array(
                    'createNewreponse' => $reponseAddNew,
                    'response' => !empty($aAttributes['extraSurveyNameInLanguage']['en']) ? $aAttributes['extraSurveyNameInLanguage']['en'] : mb_strtolower($this->translate("Response"), 'UTF-8'),
                ),
                $this->getLanguageStrings()
            ),
            'questionExtraSurveyReset' => array(
                'surveyId' => $currentSurveyId,
                'step' => $currentStep,
                'reloaded' => $reloaded,
            ),
        );
        /* @todo : reset  ExpressionManager to current */
        Template::model()->getInstance('', $oQuestion->sid);
        $this->subscribe("getPluginTwigPath", "addTwigPath");
        if ($aAttributes['extraSurveyMultipleLinks']) {
            $reponsesList = App()->twigRenderer->renderPartial(
                "/survey/questions/answer/extrasurvey/reponseslistLink.twig",
                $renderData
            );
        } else {
            $reponsesList = App()->twigRenderer->renderPartial(
                "/survey/questions/answer/extrasurvey/reponseslist.twig",
                $renderData
            );
        }
        return $reponsesList;
    }

    /**
     * @see event
     * just add twig directory , disable after usage
     */
    public function addTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append("add", [$viewPath]);
    }

    /**
     * Set the answer and other parameters for the system
     * @param int $surveyId
     * @param string $srid
     * @param null|string $token
     * @param null|integer $qid for attributes
     * @return array[]
     */
    private function getPreviousResponse($surveyId, $srid, $token = null, $qid = null)
    {
        if (!$qid) {
            return array();
        }
        $oQuestion = Question::model()->find("qid = :qid", [":qid" => $qid]);
        if (!$oQuestion) {
            return array();
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['extraSurvey']) || $aAttributes['extraSurvey'] != $surveyId) {
            return array();
        }
        LimeExpressionManager::SetSurveyId($oQuestion->sid);
        $aSelect = array(
            'id',
            'submitdate'
        );
        if ($token) {
            $aSelect[] = 'token';
        }


        $qCodeText = isset($aAttributes['extraSurveyQuestion']) ? trim($aAttributes['extraSurveyQuestion']) : "";
        $showId = trim($aAttributes['extraSurveyShowId']);
        $orderBy = isset($aAttributes['extraSurveyOrderBy']) ? trim($aAttributes['extraSurveyOrderBy']) : null;
        $qCodeSrid = trim($aAttributes['extraSurveyQuestionLink']);
        $allowDelete = $aAttributes['extraSurveyQuestionAllowDelete'] ?? false;
        /** @var array all columns */
        $availableColumns = SurveyDynamic::model($surveyId)->getAttributes();
        /** @var null|array question code as key, column as value */
        $aEmToColumns = null;
        if (Yii::getPathOfAlias('getQuestionInformation')) {
            $aEmToColumns = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId));
        }
        $tokenUsage = $aAttributes['extraSurveyTokenUsage'];
        $aOtherFields = $this->getOtherField($qid);
        if (!$aAttributes['extraSurveyQuestionLinkUse']) {
            $qCodeSrid = null;
        }
        /* Find the question code */
        $oQuestionText = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $surveyId,":title" => $qCodeText));
        $qCodeText = null;
        if ($oQuestionText && in_array($oQuestionText->type, array("S","T","U","L","!","O","N","D","G","Y","*"))) {
            $qCodeText = "{$oQuestionText->sid}X{$oQuestionText->gid}X{$oQuestionText->qid}";
            $aSelect[] = App()->db->quoteColumnName($qCodeText);
        }
        /** Add optional question by code */
        $extraQuestionsInView = isset($aAttributes['extraSurveyExtraQuestionInView']) ? trim($aAttributes['extraSurveyExtraQuestionInView']) : null;
        $aColumnsReplace = array();
        if ($extraQuestionsInView) {
            $aExtraQuestionInView = array_map('trim', explode(",", $extraQuestionsInView));
            foreach ($aExtraQuestionInView as $questionCode) {
                if (array_key_exists($questionCode, $availableColumns)) {
                    $aSelect[] = App()->db->quoteColumnName($questionCode);
                } else {
                    if ($aEmToColumns) {
                        if (isset($aEmToColumns[$questionCode])) {
                            $aSelect[] = App()->db->quoteColumnName($aEmToColumns[$questionCode]);
                            $aColumnsReplace[$aEmToColumns[$questionCode]] = $questionCode;
                        }
                    } else {
                        $oQuestionExtra = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $surveyId,":title" => $questionCode));
                        $qCodeText = null;
                        if ($oQuestionExtra && in_array($oQuestionExtra->type, array("S","T","U","L","!","O","N","D","G","Y","*"))) {
                            $aSelect[] = App()->db->quoteColumnName("{$oQuestionExtra->sid}X{$oQuestionExtra->gid}X{$oQuestionExtra->qid}");
                            $aColumnsReplace["{$oQuestionExtra->sid}X{$oQuestionExtra->gid}X{$oQuestionExtra->qid}"] = $questionCode;
                        }
                    }
                }
            }
        }
        $oCriteria = new CDbCriteria();
        $oCriteria->select = $aSelect;
        $oCriteria->condition = "";
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($surveyId);
            $oCriteria = $RelatedSurveysHelper->addFilterDeletedCriteria($oCriteria);
        }
        if ($qCodeText && !$showId) {
            $qQuotesCodeText = Yii::app()->db->quoteColumnName($qCodeText);
            $oCriteria->addCondition("$qQuotesCodeText IS NOT NULL AND $qQuotesCodeText != ''");
        }
        if ($token) {
            switch ($tokenUsage) {
                case 'no':
                    break;
                case 'group':
                    if (!$aAttributes['extraSurveyDisableReloadSettings']) {
                        \reloadAnyResponse\Utilities::setForcedAllowedSettings($surveyId, 'allowTokenGroupUser');
                    }
                    $oCriteria->addInCondition("token", $this->getTokensList($surveyId, $token));
                    break;
                case 'token':
                default:
                    $oCriteria->compare("token", $token);
            }
        }
        if ($qCodeSrid) {
            $oQuestionSrid = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $surveyId,":title" => $qCodeSrid));
            if ($oQuestionSrid && in_array($oQuestionSrid->type, array("T","S","N"))) {
                $qCodeSrid = "{$oQuestionSrid->sid}X{$oQuestionSrid->gid}X{$oQuestionSrid->qid}";
                $relationValue = $srid;
                if (boolval($aAttributes['extraSurveyMultipleLinks']) && !empty($aAttributes['extraSurveyMultipleLinksBy']) && trim($aAttributes['extraSurveyMultipleLinksBy'])) {
                    $relationValue = trim(preg_replace("/[^_.a-zA-Z0-9]/", "", $aAttributes['extraSurveyMultipleLinksBy']));
                }
                if (!$relationValue) {
                    $oCriteria->addCondition(App()->db->quoteColumnName($qCodeSrid) . " = ''");
                } else {
                    if (boolval($aAttributes['extraSurveyMultipleLinks'])) {
                        $columnQuoted = App()->db->quoteColumnName($qCodeSrid);
                        $containCriteria = new CDbCriteria();
                        $containCriteria->compare($columnQuoted, $relationValue); // IS the value
                        $containCriteria->compare($columnQuoted, $relationValue . ",%", true, 'OR', false); // Start by
                        $containCriteria->compare($columnQuoted, "%," . $relationValue . ",%", true, 'OR', false); // contain
                        $containCriteria->compare($columnQuoted, "%," . $relationValue, true, 'OR', false); // End by
                        $oCriteria->mergeWith($containCriteria);
                    } else {
                        $oCriteria->compare(App()->db->quoteColumnName($qCodeSrid), $relationValue);
                    }
                }
            }
        }
        if (!empty($aOtherFields)) {
            foreach ($aOtherFields as $questionCode => $value) {
                if (array_key_exists($questionCode, $availableColumns)) {
                    $oCriteria->compare(Yii::app()->db->quoteColumnName($questionCode), $value);
                } else {
                    if (empty($aEmToColumns)) {
                        $oQuestionOther = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $surveyId,":title" => $questionCode));
                        if ($oQuestionOther && in_array($oQuestionOther->type, array("5","D","G","I","L","N","O","S","T","U","X","Y","!","*"))) {
                            $qCode = "{$oQuestionOther->sid}X{$oQuestionOther->gid}X{$oQuestionOther->qid}";
                            $oCriteria->compare(Yii::app()->db->quoteColumnName($qCode), $value);
                        }
                    } elseif (array_key_exists($questionCode, $aEmToColumns)) {
                        $qCode = $aEmToColumns[$questionCode];
                        $oCriteria->compare(Yii::app()->db->quoteColumnName($qCode), $value);
                    }
                }
            }
        }
        $sFinalOrderBy = "";
        if (!empty($orderBy)) {
            $aOrdersBy = explode(",", $orderBy);
            $aOrderByFinal = array();
            foreach ($aOrdersBy as $sOrderBy) {
                $aOrderBy = explode(" ", trim($sOrderBy));
                $arrangement = "ASC";
                if (!empty($aOrderBy[1]) and strtoupper($aOrderBy[1]) == 'DESC') {
                    $arrangement = "DESC";
                }
                if (!empty($aOrderBy[0])) {
                    $orderColumn = null;
                    if (array_key_exists($aOrderBy[0], $availableColumns)) {
                        $aOrderByFinal[] = Yii::app()->db->quoteColumnName($aOrderBy[0]) . " " . $arrangement;
                    } elseif ($aEmToColumns) {
                        if (isset($aEmToColumns[$aOrderBy[0]])) {
                            $aOrderByFinal[] = App()->db->quoteColumnName($aEmToColumns[$aOrderBy[0]]) . " " . $arrangement;
                        }
                    }
                }
            }
            $sFinalOrderBy = implode(",", $aOrderByFinal);
        }
        if (empty($sFinalOrderBy)) {
            $sFinalOrderBy = Yii::app()->db->quoteColumnName('id') . " DESC";
            if (Survey::model()->findByPk($surveyId)->datestamp == "Y") {
                $sFinalOrderBy = Yii::app()->db->quoteColumnName('datestamp') . " ASC";
            }
        }
        $oCriteria->order = $sFinalOrderBy;
        $criteriaEvent = new PluginEvent('questionExtraSurveyGetPreviousReponseCriteria');
        $criteriaEvent->set('surveyId', $surveyId);
        $criteriaEvent->set('srid', $srid);
        $criteriaEvent->set('token', $token);
        $criteriaEvent->set('qid', $qid);
        $criteriaEvent->set('criteria', $oCriteria);
        App()->getPluginManager()->dispatchEvent($criteriaEvent);
        $oCriteria = $criteriaEvent->get('criteria');
        $oResponses = Response::model($surveyId)->findAll($oCriteria);
        $aResponses = array();
        if ($oResponses) {
            if (version_compare(\reloadAnyResponse\Utilities::API, "5.6.0", ">=")) {
                $oStartUrl = new \reloadAnyResponse\StartUrl($surveyId, $token, $tokenUsage != 'no');
            } else {
                if ($tokenUsage == 'no') {
                    $oStartUrl = new \reloadAnyResponse\StartUrl($surveyId);
                } else {
                    $oStartUrl = new \reloadAnyResponse\StartUrl($surveyId, $token);
                }
            }
            foreach ($oResponses as $oResponse) {
                $aResponses[$oResponse->id] = $oResponse->getAttributes();
                foreach ($aColumnsReplace as $column => $replace) {
                    $aResponses[$oResponse->id][$replace] = $oResponse->getAttribute($column);
                }
                /* Todo : check token related survey */
                $aResponses[$oResponse->id]['text'] = "";
                if ($qCodeText) {
                    switch ($oQuestionText->type) {
                        case "!":
                        case "L":
                            if (intval(App()->getConfig('versionnumber')) <= 3) {
                                $oAnswer = Answer::model()->find("qid=:qid and language=:language and code=:code", array(
                                    ':qid' => $oQuestionText->qid,
                                    ':language' => Yii::app()->getLanguage(),
                                    ':code' => $oResponse->getAttribute($qCodeText),
                                ));
                                if ($oAnswer) {
                                    $aResponses[$oResponse->id]['text'] .= $oAnswer->answer;
                                } else {
                                    $aResponses[$oResponse->id]['text'] .= $oResponse->$qCodeText; // Review for other
                                }
                            } else { /* version up to 3 */
                                $oAnswer = Answer::model()->resetScope()->with('answerl10ns')->find(array(
                                    'condition' => "qid=:qid and code=:code and answerl10ns.language = :language",
                                    'order' => 'sortorder, code',
                                    'params' => array(
                                        ':qid' => $oQuestionText->qid,
                                        ':language' => Yii::app()->getLanguage(),
                                        ':code' => $oResponse->getAttribute($qCodeText)
                                    )
                                ));
                                if ($oAnswer) {
                                    $aResponses[$oResponse->id]['text'] .= $oAnswer->answerl10ns[Yii::app()->getLanguage()]->answer;
                                } else {
                                    $aResponses[$oResponse->id]['text'] .= $oResponse->$qCodeText; // Review for other
                                }
                            }
                            break;
                        default:
                            $aResponses[$oResponse->id]['text'] .= $oResponse->$qCodeText;
                    }
                }
                if (empty($aResponses[$oResponse->id]['text'])) {
                    $aResponses[$oResponse->id]['text'] .= $oResponse->id;
                }
                $aResponses[$oResponse->id]['url'] = $oStartUrl->getUrl(
                    $oResponse->id,
                    array('extrasurveyqid' => $qid),
                    $tokenUsage == 'no'
                );
                $aResponses[$oResponse->id]['unlinkurl'] = null;
                if ($aResponses[$oResponse->id]['url']) {
                    $aResponses[$oResponse->id]['unlinkurl'] = Yii::app()->getController()->createUrl(
                        'plugins/direct',
                        array(
                            'plugin' => 'questionExtraSurvey',
                            'function' => 'unlink',
                            'surveyid' => $surveyId,
                            'token' => $token,
                            'qid' => $qid,
                            'linksrid' => $oResponse->id
                        )
                    );
                }

                $aResponses[$oResponse->id]['deleteurl'] = null;
                if (
                    $aResponses[$oResponse->id]['url'] && $allowDelete &&
                    version_compare(Yii::app()->getConfig('reloadAnyResponseApi'), "5.12.0", ">=") &&
                    \reloadAnyResponse\Utilities::getSetting($surveyId, 'allowDirectDelete')
                ) {
                    $aParams = [
                        'plugin' => 'reloadAnyResponse',
                        'function' => 'delete',
                        'sid' => $surveyId,
                        'srid' => $oResponse->id,
                    ];
                    if ($token) {
                        $aParams['token'] = $token;
                    }
                    $responseLink = \reloadAnyResponse\models\responseLink::model()->findByPk(['sid' => $surveyId, 'srid' => $oResponse->id]);
                    if ($responseLink && $responseLink->accesscode) {
                        $params['code'] = $responseLink->accesscode;
                    }
                    $aResponses[$oResponse->id]['deleteurl'] = Yii::app()->getController()->createUrl(
                        'plugins/direct',
                        $aParams
                    );
                }
            }
        }
        return $aResponses;
    }

    /**
     * Management of extra survey (before shown)
     */
    private function qesRegisterExtraSurveyScript()
    {
        Yii::app()->clientScript->addPackage('manageExtraSurvey', array(
            'basePath'    => 'questionExtraSurvey.assets',
            'js'          => array('extraSurvey.js'),
        ));
        Yii::app()->getClientScript()->registerPackage('manageExtraSurvey');
    }

    /**
     * Force reloadedAnyAnswer permission
     * @var integer $urveyId for destination
     * @var array $aQuestionAttributes : settings of the question
     */
    private function setReloadAnyResponseAnswer($surveyId, $aQuestionAttributes)
    {
        /* force replaceDefaultSave to always */
        \reloadAnyResponse\Utilities::setForcedAllowedSettings($surveyId, 'replaceDefaultSave', 2);
        if ($aQuestionAttributes['extraSurveyDisableReloadSettings']) {
            return;
        }
        $extraSurvey = Survey::model()->findByPk($surveyId);
        if (!$extraSurvey->getHasTokensTable() || $extraSurvey->getIsAnonymized()) {
            \reloadAnyResponse\Utilities::setForcedAllowedSettings($surveyId, 'uniqueCodeAccess');
        }
        $tokenSettings = $aQuestionAttributes['extraSurveyTokenUsage'];
        switch ($tokenSettings) {
            case 'no':
                \reloadAnyResponse\Utilities::setForcedAllowedSettings($surveyId, 'uniqueCodeAccess');
                break;
            case 'group':
                \reloadAnyResponse\Utilities::setForcedAllowedSettings($surveyId, 'allowTokenGroupUser');
                // no break
            case 'token':
            default:
                \reloadAnyResponse\Utilities::setForcedAllowedSettings($surveyId, 'allowTokenUser');
        }
    }
    /**
     * Validate if same token exist, create if not.
     * @param Survey $extraSurvey
     * @param Survey $thisSurvey
     * @return boolean (roken is valid)
     */
    private function validateToken($extraSurvey, $thisSurvey, $basetoken = null)
    {
        if (!$this->surveyAccessWithToken($extraSurvey)) {
            return false;
        }
        if (!$basetoken) {
            $basetoken = isset($_SESSION['survey_' . $thisSurvey->sid]['token']) ? $_SESSION['survey_' . $thisSurvey->sid]['token'] : null;
        }
        if (!$basetoken) {
            $this->log(sprintf("Unable to find token value for %s.", $thisSurvey->sid), 'warning');
            return false;
        }
        /* Find if token exist in new survey */
        $oToken = Token::model($extraSurvey->sid)->find("token = :token", array(":token" => $basetoken));
        if (empty($oToken)) {
            $oBaseToken = Token::model($thisSurvey->sid)->find("token = :token", array(":token" => $basetoken));
            if (empty($oBaseToken)) {
                $this->log(sprintf("Unable to create token for %s, token for %s seems invalid.", $extraSurvey->sid, $thisSurvey->sid), 'error');
                return false;
            }
            $oToken = Token::create($extraSurvey->sid);
            $disableAttribute = array("tid","participant_id","emailstatus","blacklisted","sent","remindersent","remindercount","completed","usesleft");
            $updatableAttribute = array_filter(
                $oBaseToken->getAttributes(),
                function ($key) use ($disableAttribute) {
                    return !in_array($key, $disableAttribute);
                },
                ARRAY_FILTER_USE_KEY
            );
            $oToken->setAttributes($updatableAttribute, false);
            if ($oToken->save()) {
                $this->log(sprintf("Auto create token %s for %s.", $basetoken, $extraSurvey->sid), 'info');
                return true;
            } else {
                $this->log(sprintf("Unable to create auto create token %s for %s.", $basetoken, $extraSurvey->sid), 'error');
                $this->log(CVarDumper::dumpAsString($oToken->getErrors), 'info');
                return false;
            }
        }
        return true;
    }

    /**
     * Test is survey have token table
     * @param $iSurvey
     * @return boolean
     */
    private function surveyHasToken($iSurvey)
    {
        if (version_compare(Yii::app()->getConfig('versionnumber'), "3", ">=")) {
            return Survey::model()->findByPk($iSurvey)->getHasTokensTable();
        }
        return Survey::model()->hasTokens($iSurvey);
    }

    /**
     * Return the list of token related by responseListAndManage
     * @todo : move this to a responseListAndManage helper
     * @param integer $surveyId
     * @param string $token
     * @return string[]
     */
    private function getTokensList($surveyId, $token)
    {
        $tokensList = array($token => $token);
        if (!$this->surveyHasToken($surveyId)) {
            return $tokensList;
        }
        if (version_compare(App()->getConfig('TokenUsersListAndManageAPI', 0), '0.14', ">=")) {
            return \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token, false);
        }
        if (!Yii::getPathOfAlias('responseListAndManage')) {
            return $tokensList;
        }
        if (!class_exists('\responseListAndManage\Utilities')) {
            return $tokensList;
        }
        return \responseListAndManage\Utilities::getTokensList($surveyId, $token, true);
    }

    /**
    * Did this survey have token with reload available
    * @var \Survey
    * @return boolean
    */
    private function surveyAccessWithToken($oSurvey)
    {
        Yii::import('application.helpers.common_helper', true);
        return $oSurvey->anonymized != "Y" && tableExists("{{tokens_" . $oSurvey->sid . "}}");
    }

    /**
     * Set the other field for current qid
     * @param integer $qid
     * @param string $otherField to analyse
     * @param integer $surveyId in this survey
     * @return void
     */
    private function setOtherField($qid, $otherField, $surveyId)
    {
        $aOtherFieldsLines = preg_split('/\r\n|\r|\n/', $otherField, -1, PREG_SPLIT_NO_EMPTY);
        $aOtherFields = array();
        foreach ($aOtherFieldsLines as $otherFieldLine) {
            if (!strpos($otherFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($otherFieldLine, 0, strpos($otherFieldLine, ":"));
            $value = substr($otherFieldLine, strpos($otherFieldLine, ":") + 1);
            $value = self::qesEMProcessString($value);
            $aOtherFields[$key] = $value;
        }
        App()->getSession()->add("questionExtraOtherField{$qid}", $aOtherFields);
    }

    /**
     * Set the other field for current qid
     * @param integer $qid
     * @return null|srting[]
     */
    private function getOtherField($qid)
    {
        return App()->getSession()->get("questionExtraOtherField{$qid}", null);
    }

    /**
     * Validate a question have extra survey attribute to extra sis
     * @param integer $qid
     * @param integer $extraSid
     * @return boolean
     */
    private function validateQuestionExtraSurvey($qid, $extraSid)
    {
        $title = Survey::model()->findByPk($extraSid)->getLocalizedTitle(); // @todo : get default lang title
        /* search if it's a related survey */
        $oAttributeExtraSurvey = QuestionAttribute::model()->find('attribute=:attribute AND qid=:qid', array(
            ':attribute' => 'extraSurvey',
            ':qid' => $qid,
        ));
        /* Validate if usage of extraSurvey is OK here with current qid */
        if ($oAttributeExtraSurvey && ($oAttributeExtraSurvey->value == $extraSid || $oAttributeExtraSurvey->value == $title)) {
            return true;
        }
        return false;
    }

    /**
     * Return a json with list of response with text, response id and link to attach
     * Work only when survey is actuvate and working (habve a session)
     * @param integer $qid the questoon for settings
     * @param integer $surveyid
     * @param string $term
     * @return void
     */
    private function returnSearchResponseListForLink($qid, $currentSurveyId, $sourceSurveyid, $term)
    {
        $result = array();
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($currentSurveyId);
        if (empty($srid)) {
            $this->throwJsonSelect2Exception($this->translate("Unable to get responses."));
        }
        $oRelatedSurvey = Survey::model()->findByPk($sourceSurveyid);
        if (!$oRelatedSurvey->isActive) {
            $this->throwJsonSelect2Exception($this->translate("Related survey is not started."));
        }
        $aQuestionAttributes = QuestionAttribute::model()->getQuestionAttributes($qid, Yii::app()->getLanguage());
        if (!$oRelatedSurvey->isAnonymized) {
            $token = \reloadAnyResponse\Utilities::getCurrentToken($currentSurveyId);
            $tokenUsage = $aQuestionAttributes['extraSurveyTokenUsage'];
            if (empty($token) && $oRelatedSurvey->hasTokensTable && $tokenUsage != 'no') {
                $this->throwJsonSelect2Exception($this->translate("Related survey need token."));
            }
        }
        $criteria = new CDbCriteria();
        $aSelect = [
            'id'
        ];
        /* Get the prefill part*/
        $qCodeSrid = trim($aQuestionAttributes['extraSurveyQuestionLink']);
        $oQuestionSrid = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array( ":sid" => $sourceSurveyid, ":title" => $qCodeSrid));
        if ($oQuestionSrid && in_array($oQuestionSrid->type, array("T","S","N"))) {
            $qCodeSrid = "{$oQuestionSrid->sid}X{$oQuestionSrid->gid}X{$oQuestionSrid->qid}";
            $aSelect[] = $qCodeSrid;
        } else {
            $this->throwJsonSelect2Exception($this->translate("Invalid question parameters, no question for response id."));
        }
        /* The text to return */
        $qCodeText = isset($aQuestionAttributes['extraSurveyQuestion']) ? trim($aQuestionAttributes['extraSurveyQuestion']) : "";
        $oQuestionText = Question::model()->find(
            "sid=:sid and title=:title and parent_qid=0",
            array(":sid" => $sourceSurveyid, ":title" => $qCodeText)
        );
        if (empty($oQuestionText) || !in_array($oQuestionText->type, array("S","T","U","L","!","O","N","D","G","Y","*"))) {
            $this->throwJsonSelect2Exception($this->translate("No valid extraSurveyQuestion set."));
        }
        /* @var the code for prefilling */
        $qCodePrefill = $qCodeText;
        if (trim($aQuestionAttributes['extraSurveyQuestionLinkCreate'])) {
            $qCodePrefill = trim($aQuestionAttributes['extraSurveyQuestionLinkCreate']);
        }
        /* @var the column name for search */
        $qCodeText = "{$oQuestionText->sid}X{$oQuestionText->gid}X{$oQuestionText->qid}";
        $aSelect[] = App()->db->quoteColumnName($qCodeText);
        $criteria->condition = App()->db->quoteColumnName($qCodeText) . " IS NOT NULL AND " . App()->db->quoteColumnName($qCodeText) . " != ''";
        if ($term) {
            $criteria->compare($qCodeText, $term, true);
        }
        $relationValue = $srid;
        if (boolval($aQuestionAttributes['extraSurveyMultipleLinks']) && !empty($aQuestionAttributes['extraSurveyMultipleLinksBy']) && trim($aQuestionAttributes['extraSurveyMultipleLinksBy'])) {
            $relationValue = trim(preg_replace("/[^_.a-zA-Z0-9]/", "", $aQuestionAttributes['extraSurveyMultipleLinksBy']));
        }
        if (!empty($token)) {
            switch ($tokenUsage) {
                case 'no':
                    break;
                case 'group':
                    $aSelect[] = 'token';
                    $criteria->addInCondition("token", $this->getTokensList($sourceSurveyid, $token));
                    break;
                case 'token':
                default:
                    $aSelect[] = 'token';
                    $criteria->compare("token", $token);
            }
        }
        /** @var array all columns */
        $availableColumns = SurveyDynamic::model($sourceSurveyid)->getAttributes();
        /** @var null|array question code as key, column as value */
        $aEmToColumns = null;
        if (Yii::getPathOfAlias('getQuestionInformation')) {
            $aEmToColumns = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($sourceSurveyid));
        }
        /** Add optional question by code */
        $extraQuestionsInView = isset($aQuestionAttributes['extraSurveyExtraQuestionInView']) ? trim($aQuestionAttributes['extraSurveyExtraQuestionInView']) : null;
        $aColumnsReplace = array();
        if ($extraQuestionsInView) {
            $aExtraQuestionInView = array_map('trim', explode(",", $extraQuestionsInView));
            foreach ($aExtraQuestionInView as $questionCode) {
                if (array_key_exists($questionCode, $availableColumns)) {
                    $aSelect[] = App()->db->quoteColumnName($questionCode);
                } else {
                    if ($aEmToColumns) {
                        if (isset($aEmToColumns[$questionCode])) {
                            $aSelect[] = App()->db->quoteColumnName($aEmToColumns[$questionCode]);
                            $aColumnsReplace[$aEmToColumns[$questionCode]] = $questionCode;
                        }
                    } else {
                        $oQuestionExtra = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $sourceSurveyid,":title" => $questionCode));
                        $qCodeText = null;
                        if ($oQuestionExtra && in_array($oQuestionExtra->type, array("S","T","U","L","!","O","N","D","G","Y","*"))) {
                            $aSelect[] = App()->db->quoteColumnName("{$oQuestionExtra->sid}X{$oQuestionExtra->gid}X{$oQuestionExtra->qid}");
                            $aColumnsReplace["{$oQuestionExtra->sid}X{$oQuestionExtra->gid}X{$oQuestionExtra->qid}"] = $questionCode;
                        }
                    }
                }
            }
        }
        /* Other field */
        $aOtherFields = $this->getOtherField($qid);
        if (!empty($aOtherFields)) {
            foreach ($aOtherFields as $questionCode => $value) {
                $oQuestionOther = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array(":sid" => $sourceSurveyid,":title" => $questionCode));
                if ($oQuestionOther && in_array($oQuestionOther->type, array("5","D","G","I","L","N","O","S","T","U","X","Y","!","*"))) {
                    $qCode = "{$oQuestionOther->sid}X{$oQuestionOther->gid}X{$oQuestionOther->qid}";
                    $criteria->compare(App()->db->quoteColumnName($qCode), $value);
                }
            }
        }
        /* @var string the final ordering */
        $sFinalOrderBy = "";
        if (!empty($orderBy)) {
            $aOrdersBy = explode(",", $orderBy);
            $aOrderByFinal = array();
            foreach ($aOrdersBy as $sOrderBy) {
                $aOrderBy = explode(" ", trim($sOrderBy));
                $arrangement = "ASC";
                if (!empty($aOrderBy[1]) and strtoupper($aOrderBy[1]) == 'DESC') {
                    $arrangement = "DESC";
                }
                if (!empty($aOrderBy[0])) {
                    $orderColumn = null;
                    if (array_key_exists($aOrderBy[0], $availableColumns)) {
                        $aOrderByFinal[] = App()->db->quoteColumnName($aOrderBy[0]) . " " . $arrangement;
                    } elseif ($aEmToColumns) {
                        if (isset($aEmToColumns[$aOrderBy[0]])) {
                            $aOrderByFinal[] = App()->db->quoteColumnName($aEmToColumns[$aOrderBy[0]]) . " " . $arrangement;
                        }
                    }
                }
            }
            $sFinalOrderBy = implode(",", $aOrderByFinal);
        }
        if (empty($sFinalOrderBy)) {
            $sFinalOrderBy = App()->db->quoteColumnName($qCodeText) . " ASC";
        }
        $criteria->select = $aSelect;
        $criteria->order = $sFinalOrderBy;
        $oResponses = Response::model($sourceSurveyid)->findAll($criteria);
        $items = array();
        if (empty($oResponses)) {
            $newUrlParam = array(
                'sid' => $sourceSurveyid,
                'extrasurveyqid' => $qid,
                'newtest' => 'Y',
                'token' => $token,
                'srid' => 'new',
                'lang' => Yii::app()->getLanguage(),
                $qCodeSrid => $relationValue,
                $qCodePrefill => $term
            );

            if (!empty($aOtherFields)) {
                foreach ($aOtherFields as $key => $value) {
                    $newUrlParam[$key] = $value;
                }
            }
            $urlNewLink = App()->getController()->createUrl(
                'survey/index',
                $newUrlParam
            );
            $items[] = [
                'id' => 0,
                'text' => sprintf($this->translate("%s (Create)"), $term),
                'action' => array(
                    'create' => $urlNewLink
                )
            ];
        } else {
            foreach ($oResponses as $oResponse) {
                $urlLink = Yii::app()->getController()->createUrl(
                    'plugins/direct',
                    array(
                        'plugin' => 'questionExtraSurvey',
                        'function' => 'dolink',
                        'surveyid' => $sourceSurveyid,
                        'token' => $token,
                        'qid' => $qid,
                        'linksrid' => $oResponse->id
                    )
                );
                $urlEditLink = Yii::app()->getController()->createUrl(
                    'plugins/direct',
                    array(
                        'plugin' => 'questionExtraSurvey',
                        'function' => 'dolink',
                        'surveyid' => $sourceSurveyid,
                        'token' => $token,
                        'qid' => $qid,
                        'linksrid' => $oResponse->id,
                        'edit' => 1
                    )
                );
                $disable = false;
                $currentSrids = explode(',', $oResponse->getAttribute($qCodeSrid));
                if (in_array($relationValue, $currentSrids)) {
                    $disable = true;
                }
                $response = array(
                    'id' => $oResponse->id,
                    'text' => $oResponse->getAttribute($qCodeText),
                    'action' => array(
                        'link' => $urlLink,
                        'linkedit' => $urlEditLink
                    ),
                    'disabled' => $disable,
                );
                foreach ($aColumnsReplace as $column => $replace) {
                     $response[$replace] = $oResponse->getAttribute($column);
                }
                $items[] = $response;
            }
        }
        $returnResponsesListEvent = new PluginEvent('questionExtraSurveyReturnResponsesList');
        $returnResponsesListEvent->set('surveyId', $sourceSurveyid);
        $returnResponsesListEvent->set('parentSurveyId', $currentSurveyId);
        $returnResponsesListEvent->set('extrasurveyqid', $qid);
        $returnResponsesListEvent->set('token', $token);
        $returnResponsesListEvent->set('relationKeyCode', $qCodeSrid);
        $returnResponsesListEvent->set('relationValue', $relationValue);
        $returnResponsesListEvent->set('prefillKeyCode', $qCodePrefill);
        $returnResponsesListEvent->set('term', $term);
        $returnResponsesListEvent->set('items', $items);
        $returnResponsesListEvent->set('responsesCount', count($oResponses));
        $returnResponsesListEvent->set('aOtherFields', $aOtherFields);
        $returnResponsesListEvent->set('extraQuestionsInView', $extraQuestionsInView);
        $returnResponsesListEvent->set('aQuestionAttributes', $aQuestionAttributes);
        App()->getPluginManager()->dispatchEvent($returnResponsesListEvent);
        $items = $returnResponsesListEvent->get('items');

        $this->displayJson(array(
            'total_counts' => count($items),
            'items' => $items
        ));
    }

    /**
     * Create a link bewteen a response
     * @param integer $qid the question for settings
     * @param integer $currentSurveyId
     * @param integer $destinationSurveyid
     * @param integer $destinationSrid
     * @return void
     */
    private function actionDoLink($qid, $currentSurveyId, $destinationSurveyid, $destinationSrid)
    {
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($currentSurveyId);
        if (empty($srid)) {
            $this->throwJsonSelect2Exception($this->translate("Unable to get responses."));
        }
        $oRelatedSurvey = Survey::model()->findByPk($destinationSurveyid);
        if (!$oRelatedSurvey->isActive) {
            $this->throwJsonSelect2Exception($this->translate("Related survey is not started."));
        }
        $aQuestionAttributes = QuestionAttribute::model()->getQuestionAttributes($qid, Yii::app()->getLanguage());
        if (!$oRelatedSurvey->isAnonymized) {
            $token = \reloadAnyResponse\Utilities::getCurrentToken($currentSurveyId);
            $tokenUsage = $aQuestionAttributes['extraSurveyTokenUsage'];
            if (empty($token) && $oRelatedSurvey->hasTokensTable && $tokenUsage != 'no') {
                $this->throwJsonSelect2Exception($this->translate("Related survey need token."));
            }
        }
        $criteria = new CDbCriteria();
        $aSelect = [
            'id'
        ];
        if (!empty($token)) {
            switch ($tokenUsage) {
                case 'no':
                    break;
                case 'group':
                    $aSelect[] = 'token';
                    $criteria->addInCondition("token", $this->getTokensList($destinationSurveyid, $token));
                    break;
                case 'token':
                default:
                    $aSelect[] = 'token';
                    $criteria->compare("token", $token);
            }
        }
        $qCodeSrid = trim($aQuestionAttributes['extraSurveyQuestionLink']);
        $oQuestionSrid = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array( ":sid" => $destinationSurveyid, ":title" => $qCodeSrid));
        if ($oQuestionSrid && in_array($oQuestionSrid->type, array("T","S","N"))) {
            $qCodeSrid = "{$oQuestionSrid->sid}X{$oQuestionSrid->gid}X{$oQuestionSrid->qid}";
            $aSelect[] = $qCodeSrid;
        } else {
            $this->throwJsonSelect2Exception($this->translate("Invalid question parameters, no question for response id."));
        }
        $criteria->select = $aSelect;
        $oResponse = Response::model($destinationSurveyid)->findByPk(
            $destinationSrid,
            $criteria
        );
        if (empty($oResponse)) {
            $this->throwJsonSelect2Exception($this->translate("Invalid response to update."));
        }
        $relationValue = $srid;
        if (boolval($aQuestionAttributes['extraSurveyMultipleLinks']) && !empty($aQuestionAttributes['extraSurveyMultipleLinksBy']) && trim($aQuestionAttributes['extraSurveyMultipleLinksBy'])) {
            $relationValue = trim($aQuestionAttributes['extraSurveyMultipleLinksBy']);
        }
        if (empty($relationValue)) {
            $this->throwJsonSelect2Exception($this->translate("Invalid link value set for question in link."));
        }
        $currentSrids = explode(',', $oResponse->getAttribute($qCodeSrid));
        $currentSrids[] = $relationValue;
        $currentSrids = array_unique(array_filter($currentSrids));
        asort($currentSrids);
        $updated = Response::model($destinationSurveyid)->updateByPk(
            $destinationSrid,
            [$qCodeSrid => implode(',', $currentSrids)]
        );
        $surveyurl = null;
        if (App()->getRequest()->getParam('edit')) {
            if (version_compare(\reloadAnyResponse\Utilities::API, "5.6.0", ">=")) {
                $oStartUrl = new \reloadAnyResponse\StartUrl($destinationSurveyid, $token, $tokenUsage != 'no');
            } else {
                if ($tokenUsage == 'no') {
                    $oStartUrl = new \reloadAnyResponse\StartUrl($destinationSurveyid);
                } else {
                    $tokenUsage = new \reloadAnyResponse\StartUrl($destinationSurveyid, $token);
                }
            }
            $surveyurl = $oStartUrl->getUrl(
                $oResponse->id,
                array('extrasurveyqid' => $qid),
                $tokenUsage == 'no'
            );
        }
        $this->displayJson([
            'result' => 'success',
            'count' => $updated,
            'surveyurl' => $surveyurl
        ]);
    }

    /**
     * Remove a link bewteen a response
     * @param integer $qid the question for settings
     * @param integer $currentSurveyId
     * @param integer $destinationSurveyid
     * @param integer $destinationSrid
     * @return void
     */
    private function actionUnLink($qid, $currentSurveyId, $destinationSurveyid, $destinationSrid)
    {
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($currentSurveyId);
        if (empty($srid)) {
            $this->throwJsonSelect2Exception($this->translate("Unable to get responses."));
        }
        $oRelatedSurvey = Survey::model()->findByPk($destinationSurveyid);
        if (!$oRelatedSurvey->isActive) {
            $this->throwJsonSelect2Exception($this->translate("Related survey is not started."));
        }
        $aQuestionAttributes = QuestionAttribute::model()->getQuestionAttributes($qid, Yii::app()->getLanguage());
        if (!$oRelatedSurvey->isAnonymized) {
            $token = \reloadAnyResponse\Utilities::getCurrentToken($currentSurveyId);
            $tokenUsage = $aQuestionAttributes['extraSurveyTokenUsage'];
            if (empty($token) && $oRelatedSurvey->hasTokensTable && $tokenUsage != 'no') {
                $this->throwJsonSelect2Exception($this->translate("Related survey need token."));
            }
        }
        $criteria = new CDbCriteria();
        $aSelect = [
            'id'
        ];
        if (!empty($token)) {
            switch ($tokenUsage) {
                case 'no':
                    break;
                case 'group':
                    $aSelect[] = 'token';
                    $criteria->addInCondition("token", $this->getTokensList($destinationSurveyid, $token));
                    break;
                case 'token':
                default:
                    $aSelect[] = 'token';
                    $criteria->compare("token", $token);
            }
        }
        $qCodeSrid = trim($aQuestionAttributes['extraSurveyQuestionLink']);
        $oQuestionSrid = Question::model()->find("sid=:sid and title=:title and parent_qid=0", array( ":sid" => $destinationSurveyid, ":title" => $qCodeSrid));
        if ($oQuestionSrid && in_array($oQuestionSrid->type, array("T","S","N"))) {
            $qCodeSrid = "{$oQuestionSrid->sid}X{$oQuestionSrid->gid}X{$oQuestionSrid->qid}";
            $aSelect[] = $qCodeSrid;
        } else {
            $this->throwJsonSelect2Exception($this->translate("Invalid question parameters, no question for response id."));
        }
        $criteria->select = $aSelect;
        $oResponse = Response::model($destinationSurveyid)->findByPk(
            $destinationSrid,
            $criteria
        );
        if (empty($oResponse)) {
            $this->throwJsonSelect2Exception($this->translate("Invalid response to update."));
        }
        $currentSrids = explode(',', $oResponse->getAttribute($qCodeSrid));
        $relationValue = $srid;
        if (boolval($aQuestionAttributes['extraSurveyMultipleLinks']) && !empty($aQuestionAttributes['extraSurveyMultipleLinksBy']) && trim($aQuestionAttributes['extraSurveyMultipleLinksBy'])) {
            $relationValue = trim($aQuestionAttributes['extraSurveyMultipleLinksBy']);
        }
        /* remove the $srid */
        if (in_array($relationValue, $currentSrids)) {
            unset($currentSrids[array_search($relationValue, $currentSrids)]);
        }
        $currentSrids = array_unique(array_filter($currentSrids));
        asort($currentSrids);
        $updated = Response::model($destinationSurveyid)->updateByPk(
            $destinationSrid,
            [$qCodeSrid => implode(',', $currentSrids)]
        );
        $this->displayJson([
            'result' => 'success',
            'count' => $updated
        ]);
    }

    /**
     * Register needed packahge for public views
     * @return void
     */
    private function registerPackage()
    {
        if (App()->getClientScript()->hasPackage('questionExtraSurveyManage')) {
            return;
        }
        $package = [
            'basePath'    => 'questionExtraSurvey.assets',
            'css'         => array('questionExtraSurvey.css'),
            'js'          => array('questionExtraSurvey.js'),
        ];
        if (Yii::app()->clientScript->hasPackage('bootstrap-select2')) {
            $package['depends'] = ['bootstrap-select2'];
        }
        App()->clientScript->addPackage('questionExtraSurveyManage', $package);
        App()->getClientScript()->registerPackage('questionExtraSurveyManage');
    }

    /*******************************************************
     * Common for a lot of plugin, helper for compatibility
     *******************************************************/

    /**
     * Process a string via expression manager (static way) and API independant
     * @param string $string
     * @param boolean $static
     * @return string
     */
    private static function qesEMProcessString($string, $static = true)
    {
        $replacementFields = array();
        if (intval(Yii::app()->getConfig('versionnumber')) < 3) {
            return \LimeExpressionManager::ProcessString($string, null, $replacementFields, false, 3, 0, false, false, $static);
        }
        if (version_compare(Yii::app()->getConfig('versionnumber'), "3.6.2", "<")) {
            return \LimeExpressionManager::ProcessString($string, null, $replacementFields, 3, 0, false, false, $static);
        }
        return \LimeExpressionManager::ProcessStepString($string, $replacementFields, 3, $static);
    }

    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable(array($this, 'gT'))) {
            return $this->gT($string, $sEscapeMode, $sLanguage);
        }
        return $string;
    }

    /**
     * return a json string as json and end.
     * @param mixed anything $data
     * return void
     */
    private function displayJson($data)
    {
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        Yii::app()->end();
    }

    /**
     * Throw an http 400 error with json message for select2
     * @param string $message
     * return void
     */
    private function throwJsonSelect2Exception($message)
    {

        $data = array(
            'total_counts' => 1,
            'items' => [
                [
                    'id' => 0,
                    'text' => $message,
                    'error' => true
                ]
            ]
        );
        header($_SERVER["SERVER_PROTOCOL"] . " 400 Bad Request", true, 400);
        $this->displayJson($data);
    }

    /**
     * All strings used for translation
     * @return string[]
     */
    private function getLanguageStrings()
    {
        return  array(
            'Add a new %s' => $this->translate("Add a new %s"),
            'Link' => $this->translate("Link"),
            'Link and edit' => $this->translate("Link and edit"),
            'Edit' => $this->translate("Edit"),
            'Close' => $this->translate("Close"),
            'Create' => $this->translate("create"),
            'Create a new %s' => $this->translate("Create a new %s"),
            'Unlink' => $this->translate("Unlink"),
            'Are you sure you want to unlink this %s' => $this->translate("Are you sure you want to unlink this %s"),
            'Not completed' => $this->translate("Not completed"),
            'Completed' => $this->translate("Completed"),
        );
    }

    /**
     * allow to fix some DB value when update
     * - 0 to 1 : remove extraSurveyResponseListAndManage usage to extraSurveyTokenUsage : if null, set it to 'group'
     * @return void
     * @throws Exception
     */
    private function fixDbByVersion()
    {
        $currentDbVersion = $this->get("dbVersion", null, null, 0);
        if ($currentDbVersion >= self::$DBversion) {
            return;
        }
        if ($currentDbVersion < 1) {
            /* Get all qid with extraSurvey set */
            $oQuestionsAttributeExtraSurvey = QuestionAttribute::model()->findAll("attribute = :attribute", array(":attribute" => "extraSurvey"));
            foreach ($oQuestionsAttributeExtraSurvey as $oQuestionExtraSurvey) {
                $oAttributeResponseListAndManage = QuestionAttribute::model()->find(
                    "qid = :qid AND attribute = :attribute",
                    array(
                        ":qid" => $oQuestionExtraSurvey->qid,
                        ":attribute" => "extraSurveyResponseListAndManage",
                    )
                );
                if (empty($oAttributeResponseListAndManage)) {
                    $oAttributeResponseTokenUsage = QuestionAttribute::model()->find(
                        "qid = :qid AND attribute = :attribute",
                        array(
                            ":qid" => $oQuestionExtraSurvey->qid,
                            ":attribute" => "extraSurveyTokenUsage",
                        )
                    );
                    if (empty($oAttributeResponseTokenUsage)) {
                        $oAttributeResponseTokenUsage = new QuestionAttribute();
                        $oAttributeResponseTokenUsage->qid = $oQuestionExtraSurvey->qid;
                        $oAttributeResponseTokenUsage->attribute = 'extraSurveyTokenUsage';
                        $oAttributeResponseTokenUsage->value = 'group';
                        $oAttributeResponseTokenUsage->save();
                    }
                }
                if (!empty($oAttributeResponseListAndManage)) {
                    $oAttributeResponseListAndManage->delete();
                }
            }
            $this->set("dbVersion", 1);
        }
        $this->set("dbVersion", self::$DBversion);
    }

    /**
    * @inheritdoc adding string, by default current event
    * @param string $message
    * @param string $level From CLogger, defaults to CLogger::LEVEL_TRACE
    * @param string $logDetail
    */
    public function log($message, $level = \CLogger::LEVEL_TRACE, $logDetail = null)
    {
        if (!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        if ($logDetail) {
            $logDetail = "." . $logDetail;
        }
        $category = get_class($this);
        \Yii::log($message, $level, 'plugin.' . $category . $logDetail);
    }
}
