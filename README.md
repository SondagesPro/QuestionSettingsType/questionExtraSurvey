# questionExtraSurvey

Add survey inside survey : create question with list of response in another survey

## Installation

This survey **need** [reloadAnyReponse](https://gitlab.com/SondagesPro/coreAndTools/reloadAnyResponse) plugin

This plugin use
- [getQuestionInformation](https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation) for ordering easily
- [TokenUsersListAndManage](https://gitlab.com/SondagesPro/TokenManagement/TokenUsersListAndManage) or [responseListAndManage](https://gitlab.com/SondagesPro/managament/responseListAndManage) for group of tokens.
- [autoSaveAndQuit](https://gitlab.com/SondagesPro/coreAndTools/autoSaveAndQuit) can be interesting if you use it with group of tokens or allowing multiple edition of a survey.

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/questionExtraSurvey directory `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/questionExtraSurvey.git questionExtraSurvey`

### Via ZIP dowload
- Download <https://dl.sondages.pro/questionExtraSurvey.zip>
- Extract : `unzip questionExtraSurvey.zip`
- Move the directory to  plugins/ directory inside LimeSUrvey

## Usage

Create a survey to shown inside another survey, can be

### All question advanced settings

- `extraSurvey` The related survey id.
- `extraSurveyQuestionLink` : The optionnal question code in the extra survey to be used to be filled by the cirrent response id when create a new remated survey.
- `extraSurveyQuestionLinkUse` : You can choose to use the previous question and return only extra survey related to this response or return all survey with other relation.
- `extraSurveyTokenUsage` : Search only survey with current token, by default usage of token. You can choose to not use token or to use group from responseListAndManage plugin.
- `extraSurveyDisableReloadSettings` : By default reloadAnyResponse plugin settings are forced to allow reload by code, token or group token according to previous setting. You can disable this system. Used when you use extra plugins for access. 
- `extraSurveyCheckExtraRight` : With token access, you can disable checking extra permission for editing response by reloadAnyReponse. The list return 
- `extraSurveyQuestion` : The question code to use for title in listing. Without a valid question code : system show reponse id.
- `extraSurveyAutoDelete` : When survey is closed : if title is empty : delete. This work only for current survey and use a javascript solution.
- `extraSurveyOtherField` : Other fields for relation : allow more restriction and more auto filling. This allow to use same survey with different relation. Single choice question are used to create new related surey and to find related survey. The value must be used as static value. You can use multiple question to prefill value in related survey, but it's not used to find related surveys.
- `extraSurveyQuestionAllowDelete` : Add a delete button on the dialog box, can force deletion on any condition.
- `extraSurveyDeleteUnsubmitted` : Remove the close button, this not really disable unsubmitted survey. Protection is on client only.
- `extraSurveyFillAnswer` The way to fill answer. You can use list of reponse id, number or with a <code>|</code> separator.
- `extraSurveyShowId` : Youn can force to show reponse id in the list.
- `extraSurveyOrderBy` : Order by for list of response. You can use question code directly if you have getQuestionInformation.
- `extraSurveyNameInLanguage` : The string in each language to be used for «response»
- `extraSurveyAddNewInLanguage` : The button text to add a new reponse.
- `extraSurveyAutoCloseSubmit` : Way to auto close or not the dialog box when submit survey as complete
- `extraSurveyMaxresponse` : The maximum response to show the add new button. Set as 0 to disable the button, allow only to update existing response.

### Basic usage with token enables survey's

1. Create the 1st survey and add a long text question
    - The plugin is done for survey with token, not anonymous, token-based response persistence and allow edit response activated.
2. Create the second survey with
    - An hidden question to get the response identification of the first survey (for example surveyLinkSrid)
    - Your real question
3. Create and set the token to the 2 surveys with same token list. The second survey must be
    - **Not anonymous**
    - Token-based response **persistence not activated** or **Use left up to 1** (if you want to limit number of response by token)
    - If **Allow multiple responses** is not activated : when reloading : survey is set to not submitted.
4. Update the short text question settings with
    - Survey to use: the survey id of the second survey
    - Question for response id : the response code for identification (for example surveyLinkSrid)
    - You can use a question for the list : it can be text, single choice, numerci, date or equation question type.

This plugin is compatible with [responseListAndManage](https://gitlab.com/SondagesPro/managament/responseListAndManage) user group.

### Improve relations between related survey using any questions field for relation

When export, import first survey, or if you need to deactivate the first survey. When you reload previous response table : the link between reponse are totally lost.

You can use a [generateUniqId](https://gitlab.com/SondagesPro/QuestionSettingsType/generateUniqId) question for the link between surveys.

1. Create you uniqId question in the first survey (title : uniqId here)
2. Set _Other question fields for relation_ to surveyLinkSrid:{uniqId.NAOK}

You can use any question, for example : you can use TOKEN.

### Control of related responses.

You can use [Question validation equation](https://manual.limesurvey.org/Question_type_-_Long_free_text#Question_validation_equation_.28em_validation_q.29) to validate the current responses state.

With number or <code>|</code> separator : you can easily check if all responses are submitted with {intval(self.NAOK) == self.NAOK}. You can control the number of reponses are fixed or more than a fixed number with simple comparaison.

To show the number of not submitted reponses : you need to use code>|</code> separator.

- submitted response : <code>{if(strpos(self.NAOK,"|"),substr(self.NAOK,0,strpos(self.NAOK,"|")),self.NAOK)}</code>
- not submitted response : <code>{if(strpos(self.NAOK,"|"),substr(self.NAOK,sum(strpos(self.NAOK,"|"),1)),0)}</code>

### Sample

- [questionExtraSurvey with 1 related token survey and 2 different relation](https://demo.sondages.pro/217682?token=tokenTest&newtest=Y#)
- [questionExtraSurey with 2 related survey and token management system](https://demo.sondages.pro/534236?token=ozi1hlp3FIhJstn&newtest=Y)

#### Extend data and result

##### questionExtraSurveyReturnResponsesList event

This event happen when particpant search related survey for link

**Input**
- _surveyId_ the survey ID (child)
- _parentSurveyId_ the parent survey ID
- _extrasurveyqid_ the question ID with to extra sutvey
- _token_ the token if exist
- _relationKeyCode_ : the relation key (response ID question code)
- _relationValue_ : the value to put in relation
- _prefillKeyCode_: the question code for prefilling
- _term_: term for search or prefliing
- _items_: the items to return
- _responsesCount_ : count of response (0 if no responses)
- _aOtherFields_ : others fields of relation
- _extraQuestionsInView_ others question value in view
- _aQuestionAttributes_: all the questions attributes

**Possible output**
- _items_: Allow to add, remove or update items

##### questionExtraSurveyGetPreviousReponseCriteria event

Allow to update the search criteria when search for response

**Input**
- _surveyId_ : the survey ID where search is done (child)
- _srid_ current response ID (in parent survey)
- _token_ current token if exist
- _qid_ the question ID of the settings
- _criteria_ current Criteria

**Possible output**
- _criteria_: Allow to update criteria

##### Adding attributes

You can add your own attributes on translated _Extra survey_ category

##### Javascript variables an workaround

This javascript variables can be used in theme to update some way of action

- `LSvar.questionExtraSurvey.autoclickcreate` : in parent survey, when select a link for creation in response list select: create it directly
- `window.extraSurvey.timeOut` : in child survey time before closing dialog after action like submit or save

When you have multiple list on same page but hide some with javascript (with boostrap collapse for exemple). Javscriptupdate all list without a parent with ls-hidden or hidden class.
When event relevance:on or element:shown happen on this elemant : list was updated. 

## Home page & Copyright
- HomePage <https://www.sondages.pro/>
- Issues <https://gitlab.com/SondagesPro/QuestionSettingsType/questionExtraSurvey/issues>
- Professional support <https://support.sondages.pro/>
- Code repository <https://gitlab.com/SondagesPro/QuestionSettingsType/questionExtraSurvey>
- Copyright © 2017-2025 Denis Chenu <www.sondages.pro>
- Copyright © 2017-2025 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro)
