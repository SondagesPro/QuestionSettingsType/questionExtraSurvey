#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: questionExtraSurvey\n"
"POT-Creation-Date: 2024-05-31 19:13+0200\n"
"PO-Revision-Date: 2019-12-02 12:13+0100\n"
"Last-Translator: \n"
"Language-Team: Denis Chenu\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: translate\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: locale\n"

#: questionExtraSurvey.php:206 questionExtraSurvey.php:216
#: questionExtraSurvey.php:225 questionExtraSurvey.php:235
#: questionExtraSurvey.php:249 questionExtraSurvey.php:262
#: questionExtraSurvey.php:271 questionExtraSurvey.php:280
#: questionExtraSurvey.php:290 questionExtraSurvey.php:299
#: questionExtraSurvey.php:308 questionExtraSurvey.php:323
#: questionExtraSurvey.php:332 questionExtraSurvey.php:341
#: questionExtraSurvey.php:354 questionExtraSurvey.php:365
#: questionExtraSurvey.php:376 questionExtraSurvey.php:393
msgid "Extra survey"
msgstr ""

#: questionExtraSurvey.php:211
msgid "The survey id to be used, you need permission on this survey."
msgstr ""

#: questionExtraSurvey.php:212
msgid "Extra survey id"
msgstr ""

#: questionExtraSurvey.php:220
msgid ""
"The question code in the extra survey to be used. If empty : only token or "
"optionnal fields was used for the link."
msgstr ""

#: questionExtraSurvey.php:221
msgid "Question for response id"
msgstr ""

#: questionExtraSurvey.php:229
msgid ""
"Choose if you want only related to current response. If survey use token "
"persistence and allow edition, not needed and id can be different after "
"import an old response database."
msgstr ""

#: questionExtraSurvey.php:230
msgid "Get only response related to current response id."
msgstr ""

#: questionExtraSurvey.php:244
msgid ""
"If you have responseListAndManage, the response list can be found using the "
"group of current token. If the related survey have a token table : token "
"stay mandatory."
msgstr ""

#: questionExtraSurvey.php:245
msgid "Usage of token."
msgstr ""

#: questionExtraSurvey.php:257
msgid ""
"By default reload response plugin settings are forced according to this "
"option, you can disable this here and leave reloadAnyResponse default action."
msgstr ""

#: questionExtraSurvey.php:258
msgid "Disable forced reload response settings."
msgstr ""

#: questionExtraSurvey.php:266
msgid ""
"This can be text question type, single choice question type or equation "
"question type."
msgstr ""

#: questionExtraSurvey.php:267
msgid "Question code for listing."
msgstr ""

#: questionExtraSurvey.php:275
msgid ""
"When dialog box is closed, the new survey was checked, if title is empty : "
"survey was deleted. This doesn't guarantee you never have empty title, just "
"less."
msgstr ""

#: questionExtraSurvey.php:276
msgid "Delete survey without title when close."
msgstr ""

#: questionExtraSurvey.php:285
msgid ""
"One field by line, field must be a valid question code, single question only "
"for a real relation. The value is set when question is shown for the 1st "
"time, variable mus be static. Field and value are separated by colon (<code>:"
"</code>), you can use Expressiona Manager in value."
msgstr ""

#: questionExtraSurvey.php:286
msgid "Other question fields for relation."
msgstr ""

#: questionExtraSurvey.php:294
msgid ""
"Add a button to delete inside modal box, this allow user to really delete "
"the reponse."
msgstr ""

#: questionExtraSurvey.php:295
msgid "Allow delete response."
msgstr ""

#: questionExtraSurvey.php:303
msgid ""
"If a survey is unsubmitted : disallow close of dialog before submitting."
msgstr ""

#: questionExtraSurvey.php:304
msgid "Disallow close without submit."
msgstr ""

#: questionExtraSurvey.php:312
msgid "List of all answers."
msgstr ""

#: questionExtraSurvey.php:313
msgid "List of submitted answers."
msgstr ""

#: questionExtraSurvey.php:314
msgid "Float value with integer as submitted and decimal as not submitted."
msgstr ""

#: questionExtraSurvey.php:315
msgid ""
"Number of submitted and not submitted answers separate by <code>|</code>."
msgstr ""

#: questionExtraSurvey.php:318
msgid ""
"Recommended method is separator : submitted answer at left part, and not "
"submitted as right part (<code>submitted[|not-submitted]</code>).You can "
"check if all answer are submitted with <code>intval(self)==self</code>."
msgstr ""

#: questionExtraSurvey.php:319
msgid "Way for filling the answer."
msgstr ""

#: questionExtraSurvey.php:327
msgid "This shown survey without title too."
msgstr ""

#: questionExtraSurvey.php:328
msgid "Show id at end of string."
msgstr ""

#: questionExtraSurvey.php:336
#, php-format
msgid ""
"You can use %sSGQA identifier%s or question code if you have "
"getQuestionInformation plugin for the columns to be ordered. The default "
"order is ASC, you can use DESC. You can use <code>,</code> for multiple "
"order."
msgstr ""

#: questionExtraSurvey.php:337
msgid "Order by (default “id DESC”, “datestamp ASC” for datestamped surveys)"
msgstr ""

#: questionExtraSurvey.php:347
#, php-format
msgid ""
"When generate the response list : you can replace the default view by a twig "
"file in your survey there directory (%s)."
msgstr ""

#: questionExtraSurvey.php:350
msgid "Other question to be added in view."
msgstr ""

#: questionExtraSurvey.php:360
msgid "Default to “response“ (translated)"
msgstr ""

#: questionExtraSurvey.php:361
msgid "Show response as"
msgstr ""

#: questionExtraSurvey.php:371
msgid ""
"Default to “Add new response”, where response is the previous parameter "
"(translated)"
msgstr ""

#: questionExtraSurvey.php:372
msgid "Add new line text"
msgstr ""

#: questionExtraSurvey.php:380
msgid "Add information and close dialog box"
msgstr ""

#: questionExtraSurvey.php:381
msgid "Add information"
msgstr ""

#: questionExtraSurvey.php:382
msgid "Only close dialog box"
msgstr ""

#: questionExtraSurvey.php:383
msgid "No update and no javascript"
msgstr ""

#: questionExtraSurvey.php:388
msgid ""
"Dialog box are closed using javascript solution. Close after save use "
"autoSaveAndQuit plugin."
msgstr ""

#: questionExtraSurvey.php:389
msgid "Auto close when survey is submitted, cleared or closes after save."
msgstr ""

#: questionExtraSurvey.php:399
msgid ""
"Show the add button until this value is reached. This do not disable adding "
"response by other way. You can use Expression."
msgstr ""

#: questionExtraSurvey.php:400
msgid "Maximum reponse."
msgstr ""

#: questionExtraSurvey.php:405
#, php-format
msgid ""
"You can use %sexpression manager variables%s (question title for example) "
"for the value to be ordered. For the order default is ASC, you can use DESC."
msgstr ""

#: questionExtraSurvey.php:409
msgid ""
"You can use expression manager directly with question code separated by ,."
msgstr ""

#: questionExtraSurvey.php:411
msgid "You can use only single choice question or SGQA separated by ,."
msgstr ""

#: questionExtraSurvey.php:686
msgid "Your responses was saved as complete, you can close this windows."
msgstr ""

#: questionExtraSurvey.php:687
#, php-format
msgid "Your %s was saved as complete, you can close this windows."
msgstr ""

#: questionExtraSurvey.php:722
#, php-format
msgid "Invalid survey %s for question %s."
msgstr ""

#: questionExtraSurvey.php:725
#, php-format
msgid "Survey %s for question %s not activated."
msgstr ""

#: questionExtraSurvey.php:728
#, php-format
msgid "Survey %s for question %s can not be used with a survey without tokens."
msgstr ""

#: questionExtraSurvey.php:731 questionExtraSurvey.php:734
#, php-format
msgid "Survey %s for question %s need to be not anonymized."
msgstr ""

#: questionExtraSurvey.php:737
#, php-format
msgid ""
"Survey %s for question %s can not be loaded in preview mode without a valid "
"token."
msgstr ""

#: questionExtraSurvey.php:741
#, php-format
msgid "Survey %s for question %s token can not ne found or created."
msgstr ""

#: questionExtraSurvey.php:1010 questionExtraSurvey.php:1012
#: questionExtraSurvey.php:1052
#, php-format
msgid "Are you sure you want to delete this %s?"
msgstr ""

#: questionExtraSurvey.php:1058
msgid "Save and quit"
msgstr ""

#: questionExtraSurvey.php:1132
msgid "Related Survey is not activate, related response deactivated."
msgstr ""

#: questionExtraSurvey.php:1170 questionExtraSurvey.php:1226
#, php-format
msgid "Add a new %s"
msgstr ""

#: questionExtraSurvey.php:1225
msgid "Delete"
msgstr ""
