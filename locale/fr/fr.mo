��          \       �       �      �   (   �      �      �      �   :   �   A   0  �   r     r  ,   �  	   �     �     �  F   �  Z   <   Add a new %s Are you sure you want to delete this %s? Delete Extra survey Save and quit Your %s was saved as complete, you can close this windows. Your responses was saved as complete, you can close this windows. PO-Revision-Date: 2024-05-31 17:17:58+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/4.0.1
Language: fr
Project-Id-Version: questionExtraSurvey
 Ajouter une nouvelle %s Êtes-vous sûr de vouloir supprimer ce %s ? Supprimer Questionnaire complémentaire Enregistrer et fermer Ce %s est enregistré comme complet, vous pouvez ferme cette fenêtre. Vos réponses ont étaient sauvegardée et finalisées, vous pouvez fermer cette fenêtre. 