/**
 * @file questionExtraSurvey javascript system
 * @author Denis Chenu
 * @version 5.13.4
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
 */
/* jshint esversion: 6 */
$(document).on('modaliniframe:on', function (event) {
  $('#modal-questionExtraSurvey button').addClass('invisible');
});
$(document).on('modaliniframe:off', function (event) {
  $('#modal-questionExtraSurvey button').removeClass('invisible');
});
$(document).on('click', '[target="frame-questionExtraSurvey"]', function (event) {
  event.preventDefault();
  if (!$(this).closest("[data-modalparams-questionextrasurvey]").length) {
    return;
  }
  if (window.location != window.parent.location) {
    window.parent.$(window.parent.document).trigger("extrasurveyframe:opennewdialog");
  }
  // Set the buttons
  var modalbuttons = {
    "delete": false,
    "saveall": false,
    "saveall-quit": false,
    "moveprev": false,
    "movenext": true,
    "movesubmit": true,
    "close": true
  };
  var modalparams = $(this).closest("[data-modalparams-questionextrasurvey]").data('modalparams-questionextrasurvey');
  $.extend(modalbuttons, modalparams.buttons);
  $.each(modalbuttons, function (key, value) {
    if (value) {
      $("#modal-questionExtraSurvey button[data-action='" + key + "']").removeClass("hidden");
    } else {
      $("#modal-questionExtraSurvey button[data-action='" + key + "']").addClass("hidden");
    }
  });
  if (!modalbuttons.close) {
    $("#modal-questionExtraSurvey button[data-dismiss]").addClass("hidden");
  } else {
    $("#modal-questionExtraSurvey button[data-dismiss]").removeClass("hidden");
  }
  /* Update string by updated value for deleting response (allow to update more easily sentence) */
  if (typeof modalparams.confirmdeletestring === 'string') {
    $("#label-confirm-clearall").text(modalparams.confirmdeletestring);
  }
  // Set the data for close event
  $("#modal-questionExtraSurvey").data("checkwhenclose-questionextrasurvey", false);
  if ($(this).closest("[data-closecheck-questionextrasurvey]").length) {
    var checkUrl = $(this).closest("[data-closecheck-questionextrasurvey]").data('closecheck-questionextrasurvey');
    $("#modal-questionExtraSurvey").data("checkwhenclose-questionextrasurvey", checkUrl);
  }
  // OK, open it
  $('#modal-questionExtraSurvey').find('.modal-title').text($(this).text());
  $("#modal-questionExtraSurvey iframe").html("").attr('src', $(this).attr('href')).on("load", function () {
    updateViewModal();
  });
  $("#modal-questionExtraSurvey").data("questionExtraSurveyQid", modalparams.qid);
  $("#modal-questionExtraSurvey").data("extra-surveyid", modalparams.extrasurveyid);
  $("#modal-questionExtraSurvey").modal({
    show: true,
    keyboard: modalbuttons.close,
    backdrop: 'static'
  });
});

$(document).on("shown.bs.modal", "#modal-questionExtraSurvey", function (e) {
  updateHeightModalExtraSurvey("#modal-questionExtraSurvey");
});
$(document).on("show.bs.modal", "#modal-questionExtraSurvey", function (e) {
  if (window.location != window.parent.location) {
    window.parent.$(window.parent.document).trigger("modaliniframe:on");
  }
});
$(document).on("hide.bs.modal", "#modal-questionExtraSurvey", function (e) {
  if (window.location != window.parent.location) {
    window.parent.$(window.parent.document).trigger("modaliniframe:off");
  }
});
$(document).on("hide.bs.modal", '#modal-questionExtraSurvey', function (e) {
  var sridElement = $("#modal-questionExtraSurvey iframe").contents().find("#sridQuestionExtraSurvey");
  let extrasurveyid = 0;
  if ($(this).data('extra-surveyid')) {
      extrasurveyid = $(this).data('extra-surveyid');
  }
  if ($(sridElement).length == 1) {
    var srid = $(sridElement).val().trim();
    var defer = $.Deferred();
    checkIsEmpty($("#modal-questionExtraSurvey"), srid, extrasurveyid);
  } else {
    updateLists(extrasurveyid);
  }
});

/* Unload iframe */
$(document).on("hide.bs.modal", '#modal-questionExtraSurvey', function (e) {
  $("#modal-questionExtraSurvey .frame-loaded").addClass("frame-loading").removeClass("frame-loaded");
  $("#modal-questionExtraSurvey .modal-footer .btn").addClass("btn-waiting");
});
$(document).on("hidden.bs.modal", '#modal-questionExtraSurvey', function (e) {
  $("#modal-questionExtraSurvey").removeClass("frame-loaded").addClass("frame-loading");
  $("#modal-questionExtraSurvey").data("extra-surveyid",0);
});

/**
 * Delete response id if it's empty
 * @param ocject element
 * @param integer sris responde Id
 * @param integer surveyID
 */
function checkIsEmpty(element, srid, surveyid = 0) {
  if ($(element).data('checkwhenclose-questionextrasurvey')) {
    $.when($.ajax({
      url: $(element).data('checkwhenclose-questionextrasurvey'),
      data: {
        'srid': srid
      },
      type: "GET", // Todo : set to POST (add CRSF)
      dataType: "text",
      success: function (data) {
        // Nothing to do
      },
      error: function (xhr, status) {
        //TODO
      }
    })).done(function () {
      updateLists(surveyid);
    });
  }
}
/**
 * Update the html of response list
 * @param integer|null surveyid
 * @Todo : limit to needed part (same related surey)
 **/
function updateLists(surveyid) {
  $("[data-update-questionextrasurvey]").each(function () {
    var element = $(this);
    if ((!surveyid || $(this).data('questionextrasurvey-survey') == surveyid)) {
      if ($(this).closest('.ls-hidden,.hidden').length) {
        var hiddenElement = $(this).closest('.ls-hidden,.hidden');
        $(hiddenElement).on('relevance:on', function (e) {
          extraSurveyUpdateList(element);
        });
        $(hiddenElement).on('element:shown', function (e) {
          extraSurveyUpdateList(element);
        });
      } else {
        extraSurveyUpdateList(element);
      }
    }
  });
}
/**
 * update an single list
 * @param element
 */
function extraSurveyUpdateList(element) {
  $(element).addClass('list-questionextrasurvey-loading');
  $.ajax({
    url: $(element).data('update-questionextrasurvey'),
    data: {},
    type: "GET",
    dataType: "html",
    success: function (data) {
      $(element).removeClass('list-questionextrasurvey-loading');
      $(element).html(data);
    },
    error: function (xhr, status) {
      //TODO
      $(element).removeClass('list-questionextrasurvey-loading');
    }
  });
}

function updateHeightModalExtraSurvey(modal) {
  var navbarFixed = 0;
  if ($(".navbar-fixed-top").filter(":visible").length) {
    navbarFixed = $(".navbar-fixed-top").filter(":visible").outerHeight();
  }
  if (isNaN(navbarFixed)) {
    navbarFixed = 0;
  }
  var modalHeader = $(modal).find(".modal-header").outerHeight();
  var modalFooter = $(modal).find(".modal-footer").outerHeight();
  var finalHeight = Math.max(400, $(window).height() - (navbarFixed + modalHeader + modalFooter + 28));// Not less than 150px
  $(modal).find(".modal-dialog").css("margin-top", navbarFixed + 4);
  $(modal).find(".modal-body").css("height", finalHeight);
  $(modal).find(".modal-body iframe").css("height", finalHeight);
}

function updateViewModal() {
  $("#modal-questionExtraSurvey iframe").off("load");
  $("#modal-questionExtraSurvey .frame-loading").addClass("frame-loaded").removeClass("frame-loading");
  updateHeightModalExtraSurvey("#modal-questionExtraSurvey");
}
$(document).on('extrasurveyframe:on', function (event, data) {
  $("#modal-questionExtraSurvey .modal-footer button[data-action]").each(function () {
    $(this).prop('disabled', $("#extra-survey-iframe").contents().find("form#limesurvey button:submit[value='" + $(this).data('action') + "']").length < 1);
    if ($(this).data('action') == 'saveall-quit') {
      $(this).prop('disabled', $("#extra-survey-iframe").contents().find("form#limesurvey button:submit[value='saveall']").length < 1);
    }
    if ($(this).data('action') == 'delete') {
      $(this).prop('disabled', $("#extra-survey-iframe").contents().find("form#limesurvey button:submit[name='move'][value!='default']").length < 1);
    }
    // todo : add it in option $("#extra-survey-iframe").contents().find(".navigator").addClass("hidden");
  });
  $("#modal-questionExtraSurvey .modal-footer .btn").removeClass("btn-waiting");
});

$(document).on('extrasurveyframe:off', function (event, data) {
  $("#modal-questionExtraSurvey .modal-footer button[data-action]").each(function () {
    $(this).prop('disabled', true);
  });
  $("#modal-questionExtraSurvey .modal-footer button").addClass("btn-waiting");
});
$(document).on('extrasurveyframe:autoclose', function (event, data) {
  $("#modal-questionExtraSurvey").modal('hide');
});
/* Action automatic */
$(document).on('click', "#modal-questionExtraSurvey button[data-action]", function (e) {
  if ($(this).data('action') == "delete") {
    return;
  }
  if ($(this).data('action') == "saveall-quit") {
    return;
  }
  var questionExtraSurveyQid = $("#modal-questionExtraSurvey").data("questionExtraSurveyQid");
  $("#extra-survey-iframe").contents().find("form#limesurvey").append("<input type='hidden' name='questionExtraSurveyQid' value='" + questionExtraSurveyQid + "'>");
  $("#extra-survey-iframe").contents().find("form#limesurvey button:submit[value='" + $(this).data('action') + "']").last().click();
});
/* Action delete */
$(document).on('click', "#modal-questionExtraSurvey button[data-action='delete']:not('disabled')", function (event) {
  event.preventDefault();
  $("#modal-confirm-clearall-extrasurvey").show();
  $("#modal-confirm-clearall-extrasurvey .btn-confirm").off('click').on('click', function () {
    var questionExtraSurveyQid = $("#modal-questionExtraSurvey").data("questionExtraSurveyQid");
    $("#modal-questionExtraSurvey iframe").contents().find("form#limesurvey").append("<input type='hidden' name='questionExtraSurveyQid' value='" + questionExtraSurveyQid + "'>");
    $("#modal-questionExtraSurvey iframe").contents().find("#limesurvey").append("<input type='hidden' name='ExtraSurvey-delete' value='delete'>");
    $("#modal-questionExtraSurvey iframe").contents().find("#limesurvey").append("<input type='hidden' name='ExtraSurvey-delete-confirm' value='confirm'>");
    $("#modal-questionExtraSurvey iframe").contents().find("#limesurvey").submit();
    $("#modal-questionExtraSurvey .modal-footer .btn").addClass("btn-waiting");
  });
  $("#modal-confirm-clearall-extrasurvey [data-dismiss]").on('click', function () {
    // LimeSurve 3.13 have an issue with dialog box not closed …
    $("#modal-confirm-clearall-extrasurvey").hide();
  });
  return;
});
/* Action direct delete */
$(document).on('click', "[data-actionurl][data-confirm='modal-confirm-clearall-extrasurvey']:not('disabled')", function (event) {
  event.preventDefault();
  let actionUrl = $(this).data('actionurl');
  let listElement = $(this).closest("[data-update-questionextrasurvey]");
  $("#modal-confirm-clearall-extrasurvey").show();
  $("#modal-confirm-clearall-extrasurvey .btn-confirm").off('click').on('click', function () {
    $.ajax({
      url: actionUrl,
      data: LSvar.questionExtraSurvey.options.postBase,
      type: "POST",
      dataType: "json",
      success: function (data) {
        extraSurveyUpdateList(listElement);
      },
      error: function (xhr, status) {
        //TODO
      }
    });
  });
  $("#modal-confirm-clearall-extrasurvey [data-dismiss]").on('click', function () {
    // LimeSurve 3.13 have an issue with dialog box not closed …
    $("#modal-confirm-clearall-extrasurvey").hide();
  });
});
/* Action save a quit */
$(document).on('click', "#modal-questionExtraSurvey button[data-action='saveall-quit']:not('disabled')", function (e) {
  var questionExtraSurveyQid = $("#modal-questionExtraSurvey").data("questionExtraSurveyQid");
  $("#extra-survey-iframe").contents().find("form#limesurvey").append("<input type='hidden' name='questionExtraSurveyQid' value='" + questionExtraSurveyQid + "'>");
  $("#extra-survey-iframe").contents().find("form#limesurvey").append("<input type='hidden' name='autosaveandquit' value=1>");
  $("#extra-survey-iframe").contents().find("form#limesurvey button:submit[value='saveall']").last().click();
  $("#modal-questionExtraSurvey .modal-footer .btn").addClass("btn-waiting");
  return;
});

/* Linking action */
$(document).on('click', '[data-questionextrasurveymodalrelated]', function (event) {
  event.preventDefault();
  let modalselect = $("#" + $(this).data('questionextrasurveymodalrelated'));
  let selectwrapper = $(modalselect).find(".modal-select-item");
  let select = $(modalselect).find("select");
  let source = $(select).data('source');
  $(select).html("").select2({
    dropdownParent: $(selectwrapper),
    ajax: {
      url: source,
      dataType: 'json',
      delay: 250,
      processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 10) < data.total_count
          }
        };
      }
    }
  }).off('select2:select').on('select2:select', function (event) {
    // This is how I got ahold of the data
    var data = event.params.data;
    if (data.error) {
      // TODO
    } else {
      if (data.action.link) {
        $(modalselect).find('[data-action="extrasurvey-link"]').removeClass("hidden").data("url", data.action.link);
      } else {
        $(modalselect).find('[data-action="extrasurvey-link"]').addClass("hidden");
      }
      if (data.action.linkedit) {
        $(modalselect).find('[data-action="extrasurvey-linkedit"]').removeClass("hidden").attr("href", data.action.linkedit);
      } else {
        $(modalselect).find('[data-action="extrasurvey-linkedit"]').addClass("hidden");
      }
      if (data.action.create) {
        $(modalselect).find('[data-action="extrasurvey-create"]').removeClass("hidden").attr("href", data.action.create);
        if (typeof data.action.target !== 'undefined') {
            window.open(data.action.create,data.action.target);
            return;
        }
        if (LSvar.questionExtraSurvey.autoclickcreate) {
          $(modalselect).find('[data-action="extrasurvey-create"]').trigger('click');
        }
      } else {
        $(modalselect).find('[data-action="extrasurvey-create"]').addClass("hidden");
      }
    }
    if (data.id == 0) {

    } else {
      $(modalselect).find(".btn-extrasurveycreate").addClass("hidden");

    }
  });
  $(modalselect).modal('show');
});
$(document).on("hidden.bs.modal", '.modal-questionExtraSurvey-link', function (e) {
  let select = $(this).find("select");
  $(select).html(""); // unsure
  $(this).find(".btn-extrasurveylink").addClass("hidden").data("url", "");
  $(this).find(".btn-extrasurveycreate").addClass("hidden").attr("href", "#");
});

/* Click on link button */
$(document).on('click', '.modal-questionExtraSurvey-link .btn-extrasurveylink', function (event) {
  event.preventDefault();
  let url = $(this).data('url');
  if (!url) {
    url = $(this).attr('href');
  }
  console.warn(url);
  let extrasurveyid = '';
  if ($(this).data('extra-surveyid')) {
      extrasurveyid = $(this).data('extra-surveyid');
  }
  console.warn(extrasurveyid);
  let modalselect = $(this).closest(".modal-questionExtraSurvey-link");
  $(modalselect).modal('hide');
  $.ajax({
    url: url,
    dataType: 'json'
  })
    .done(function (data) {
      if (data.surveyurl) {
        $(modalselect).find("a.link-extrasurvey-linkedit").attr("href", data.surveyurl).trigger('click');
      }
      updateLists(extrasurveyid);
    })
    .fail(function (data) {
      // TODO
    });
});
$(document).on('click', '.modal-questionExtraSurvey-link .btn-extrasurveycreate', function (event) {
  let url = $(this).attr('href');
  let modalselect = $(this).closest(".modal-questionExtraSurvey-link");
  $(modalselect).modal('hide');
});
/* Click on unlink */
$(document).on('click', '.item-response-url button[data-url]', function (event) {
  event.preventDefault();
  let url = $(this).data('url');
  let reponseText = $(this).closest("[data-questionextrasurvey-reponsename]").data("questionextrasurvey-reponsename");
  let text = LSvar.questionExtraSurvey.lang["Are you sure you want to unlink this %s"].replace("%s", reponseText);
  let extrasurveyid = 0;
  if ($(this).data('extra-surveyid')) {
      extrasurveyid = $(this).data('extra-surveyid');
  }
  if ($.bsconfirm !== undefined) {
    $.bsconfirm(text, LSvar.lang.confirm, function () {
      $('#identity__bsconfirmModal').modal('hide');
      actionAjaxWithUpdateList(url, extrasurveyid);
    });
  } else {
    if (confirm(text)) {
      actionAjaxWithUpdateList(url, extrasurveyid);
    }
  }

});

/**
 * Do an ajax actipon, and update list after
 * @var string url
 * @var integer surveyId
 * @return void
 */
function actionAjaxWithUpdateList(url, surveyId = 0) {
  $.ajax({
    url: url,
    dataType: 'json'
  })
    .done(function () {
      updateLists(surveyId);
    })
    .fail(function () {
      // TODO
    });
}
