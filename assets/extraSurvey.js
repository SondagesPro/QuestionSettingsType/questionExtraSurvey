var extraSurvey = window.extraSurvey || {};
extraSurvey.timeOut = extraSurvey.timeOut || 1;
$(document).on('ready', function () {
    if (window.location != window.parent.location) {
        window.parent.$(window.parent.document).trigger("extrasurveyframe:on");
    }
});
$(window).on('beforeunload', function () {
    if (window.location != window.parent.location) {
        window.parent.$(window.parent.document).trigger("extrasurveyframe:off");
    }
});
function extraSurveyAutoclose() {
    if (window.location != window.parent.location) {
        if (extraSurvey.timeOut >= 0) {
            window.setTimeout(function () {
                window.parent.$(window.parent.document).trigger("extrasurveyframe:autoclose");
            }, extraSurvey.timeOut * 1000);
        }
    }
}
